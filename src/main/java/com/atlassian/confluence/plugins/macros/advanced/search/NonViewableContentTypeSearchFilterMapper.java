package com.atlassian.confluence.plugins.macros.advanced.search;

import com.atlassian.confluence.content.ContentType;
import com.atlassian.confluence.content.ContentTypeModuleDescriptor;
import com.atlassian.confluence.content.CustomContentSearchExtractor;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchFilterMapper;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.PluginAccessor;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.queries.TermsFilter;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Filter;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.OpenBitSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.apache.lucene.queries.ChainedFilter.ANDNOT;


public class NonViewableContentTypeSearchFilterMapper implements LuceneSearchFilterMapper<NonViewableContentTypeSearchFilter>
{
    private PluginAccessor pluginAccessor;
    private static final Logger log = LoggerFactory.getLogger(NonViewableContentTypeSearchFilterMapper.class);

    @Override
    public Filter convertToLuceneSearchFilter(NonViewableContentTypeSearchFilter nonViewableContentTypeSearchFilter)
    {
        List<BytesRef> nonViewableContentTypes = getNonViewableContentTypes();

        if (nonViewableContentTypes.isEmpty())
        {
            return new MatchAllDocsFilter();
        }

        return new ChainedFilter(new Filter[] {new TermsFilter(CustomContentSearchExtractor.FIELD_CONTENT_PLUGIN_KEY, nonViewableContentTypes)}, ANDNOT);
    }

    private static class MatchAllDocsFilter extends Filter
    {
        @Override
        public DocIdSet getDocIdSet(AtomicReaderContext context, Bits acceptDocs) throws IOException
        {
            AtomicReader reader = context.reader();

            OpenBitSet result = new OpenBitSet(reader.maxDoc());
            result.set(0, reader.maxDoc());

            return result;
        }
    }

    private List<BytesRef> getNonViewableContentTypes()
    {
        List<BytesRef> contentTypeModuleKeys = new LinkedList<BytesRef>();
        for (ContentTypeModuleDescriptor contentTypeModuleDescriptor : pluginAccessor.getEnabledModuleDescriptorsByClass(ContentTypeModuleDescriptor.class))
        {
            ContentType contentType = null;
            try
            {
                contentType = contentTypeModuleDescriptor.getModule();
            }
            catch (Exception e)
            {
                log.debug("Error creating module: " + contentTypeModuleDescriptor, e);
            }

            try
            {
                /**
                 * If:
                 * (a) there is an error instantiating the content type
                 * (b) the content type returns false for canView(User user)
                 * We want to hide documents of this content type in search results.
                 */
                if (contentType == null || !contentType.getPermissionDelegate().canView(AuthenticatedUserThreadLocal.get()))
                    contentTypeModuleKeys.add(new BytesRef(contentTypeModuleDescriptor.getCompleteKey()));
            }
            catch (Exception e) // most likely an exception thrown by canView() - perhaps UnsupportedOperationException is it has not been implemented or is not applicable
            {
                log.debug("Error: ", e);
            }
        }

        return contentTypeModuleKeys;
    }


    public void setPluginAccessor(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }
}
