package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.content.ViewContentBean;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.it.user.LoginHelper;
import net.sourceforge.jwebunit.junit.JWebUnit;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TestName;

import static com.atlassian.confluence.it.AcceptanceTestHelper.TEST_SPACE;

public class IncludeMacroBackwardCompatibleTestCase
{
    private AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;
    private LoginHelper loginHelper;

    @Before
    public void setUp() throws Exception
    {
        helper.setUp(IncludeMacroBackwardCompatibleTestCase.class, testName);
        rpc = helper.getRpc();
        loginHelper = new LoginHelper(helper.getWebTester());

        rpc.logIn(User.ADMIN);
        rpc.grantPermission(SpacePermission.VIEW, TEST_SPACE, User.TEST);
        rpc.grantPermission(SpacePermission.PAGE_EDIT, TEST_SPACE, User.TEST);
    }

    /**
     *
     * Test backward compatible with confluence-advanced-plugin 3.3.x
     *
     */
    @Test
    public void testIncludeContentFromOtherPage()
    {
        String otherPageTitle = "otherPageTitle";
        String otherPageContent = "otherPageContent";

        Page otherPage = new Page(Space.TEST, otherPageTitle, otherPageContent);
        rpc.createPage(otherPage);

        Page testIncludeContentFromOtherPage = new Page(Space.TEST, "testIncludeContentFromOtherPage",
                "<ac:structured-macro ac:name=\"include\"><ac:parameter ac:name=\"nopanel\">true</ac:parameter><ac:parameter ac:name=\"pageTitle\">" + otherPageTitle +
                        "</ac:parameter><ac:parameter ac:name=\"spaceKey\">" + Space.TEST.getKey() + "</ac:parameter></ac:structured-macro>");

        rpc.createPage(testIncludeContentFromOtherPage);

        loginHelper.logInAs(User.TEST);
        ViewContentBean.viewPage(Space.TEST, testIncludeContentFromOtherPage.getTitle());
        JWebUnit.assertTextPresent(otherPageContent);
    }

}
