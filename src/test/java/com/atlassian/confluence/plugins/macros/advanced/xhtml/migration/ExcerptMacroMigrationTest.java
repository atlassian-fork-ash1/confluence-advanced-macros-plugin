package com.atlassian.confluence.plugins.macros.advanced.xhtml.migration;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import junit.framework.TestCase;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExcerptMacroMigrationTest extends TestCase
{
    private ExcerptMacroMigration excerptMacroMigration;
    private ContentPropertyManager contentPropertyManager;
    private MacroMigration richTextMacroMigration;
    private DefaultConversionContext conversionContext;
    private Page testPage;
    private MacroDefinition excerptMacroDefinition;

    public void testBodyConvertedToRichText() throws Exception
    {
        excerptMacroMigration.migrate(excerptMacroDefinition, conversionContext);

        verify(richTextMacroMigration).migrate(excerptMacroDefinition, conversionContext);
    }

    public void testOldContentPropertyRemoved() throws Exception 
    {
        when(contentPropertyManager.getStringProperty(testPage, "confluence.excerpt")).thenReturn("test value");

        excerptMacroMigration.migrate(excerptMacroDefinition, conversionContext);
        
        verify(contentPropertyManager).removeProperty(testPage, "confluence.excerpt");
    }

    public void testNoContentProperty() throws Exception 
    {
        when(contentPropertyManager.getStringProperty(testPage, "confluence.excerpt")).thenReturn(null);

        excerptMacroMigration.migrate(excerptMacroDefinition, conversionContext);
        
        verify(contentPropertyManager, never()).removeProperty(testPage, "confluence.excerpt");
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        contentPropertyManager = mock(ContentPropertyManager.class);
        richTextMacroMigration = mock(MacroMigration.class);
        excerptMacroMigration = new ExcerptMacroMigration();
        excerptMacroMigration.setContentPropertyManager(contentPropertyManager);
        excerptMacroMigration.setRichTextMacroMigration(richTextMacroMigration);

        testPage = mock(Page.class);
        conversionContext = new DefaultConversionContext(new PageContext(testPage));
        excerptMacroDefinition = new MacroDefinition("excerpt", null, null, Collections.<String, String>emptyMap());
    }
}
