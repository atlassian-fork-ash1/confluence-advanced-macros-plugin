package com.atlassian.confluence.plugins.macros.advanced.analytics;

import com.google.common.base.Ticker;

import org.joda.time.Duration;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.joda.time.Duration.millis;

class Timer
{
    private final Ticker ticker;

    private long startTimeNanos;
    private long cumulativeDurationNanos;

    Timer(final Ticker ticker)
    {
        this.ticker = ticker;
    }

    void start()
    {
        startTimeNanos = ticker.read();
    }

    void stop()
    {
        cumulativeDurationNanos += (ticker.read() - startTimeNanos);
    }

    Duration duration()
    {
        return millis(MILLISECONDS.convert(cumulativeDurationNanos, NANOSECONDS));
    }
}
