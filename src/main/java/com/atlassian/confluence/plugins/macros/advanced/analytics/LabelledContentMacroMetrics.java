package com.atlassian.confluence.plugins.macros.advanced.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageRequest;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.event.api.EventPublisher;

import com.google.common.base.Ticker;

import org.joda.time.Duration;

@EventName ("confluence.macro.metrics.contentbylabel")
public class LabelledContentMacroMetrics
{
    private final int maxResults;
    private final int resultsCount;
    private final Duration contentSearchDuration;
    private final Duration fetchContentEntitiesDuration;
    private final Duration templateRenderDuration;

    private LabelledContentMacroMetrics(final Builder builder)
    {
        this.maxResults = builder.maxResults;
        this.resultsCount = builder.resultsCount;
        this.contentSearchDuration = builder.contentSearchTimer.duration();
        this.fetchContentEntitiesDuration = builder.fetchContentEntitiesTimer.duration();
        this.templateRenderDuration = builder.templateRenderTimer.duration();
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public int getResultsCount()
    {
        return resultsCount;
    }

    public long getContentSearchMillis()
    {
        return contentSearchDuration.getMillis();
    }

    public long getFetchContentEntitiesMillis()
    {
        return fetchContentEntitiesDuration.getMillis();
    }

    public long getTemplateRenderMillis()
    {
        return templateRenderDuration.getMillis();
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private final Ticker ticker = Ticker.systemTicker();

        private int maxResults;
        private int resultsCount;
        private Timer contentSearchTimer = new Timer(ticker);
        private Timer fetchContentEntitiesTimer = new Timer(ticker);
        private Timer templateRenderTimer = new Timer(ticker);

        public LabelledContentMacroMetrics build()
        {
            return new LabelledContentMacroMetrics(this);
        }

        public Builder contentSearchStart(final PageRequest pageRequest)
        {
            maxResults = pageRequest.getLimit();
            contentSearchTimer.start();
            return this;
        }

        public Builder contentSearchFinish(final PageResponse<Content> response)
        {
            contentSearchTimer.stop();
            resultsCount = response.size();
            return this;
        }

        public Builder fetchContentEntitiesStart()
        {
            fetchContentEntitiesTimer.start();
            return this;
        }

        public Builder fetchContentEntitiesFinish()
        {
            fetchContentEntitiesTimer.stop();
            return this;
        }

        public Builder templateRenderStart()
        {
            templateRenderTimer.start();
            return this;
        }

        public Builder templateRenderFinish()
        {
            templateRenderTimer.stop();
            return this;
        }

        public void publish(final EventPublisher eventPublisher)
        {
            eventPublisher.publish(build());
        }
    }
}
