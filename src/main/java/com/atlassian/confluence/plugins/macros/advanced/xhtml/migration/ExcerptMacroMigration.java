package com.atlassian.confluence.plugins.macros.advanced.xhtml.migration;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.NastyUtilitiesWhichWillBeMadeRedundant;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

public class ExcerptMacroMigration implements MacroMigration
{
    private ContentPropertyManager contentPropertyManager;
    private MacroMigration richTextMacroMigration;

    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        PageContext pageContext = NastyUtilitiesWhichWillBeMadeRedundant.extractPageContext(conversionContext);

        ContentEntityObject entity = pageContext.getEntity();
        if (entity != null && contentPropertyManager.getStringProperty(entity, "confluence.excerpt") != null)
        {
            contentPropertyManager.removeProperty(entity, "confluence.excerpt");
        }

        return richTextMacroMigration.migrate(macroDefinition, conversionContext);
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setRichTextMacroMigration(MacroMigration richTextMacroMigration)
    {
        this.richTextMacroMigration = richTextMacroMigration;
    }
}
