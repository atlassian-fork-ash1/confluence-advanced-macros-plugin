package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PopularLabelsTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        deleteSpace(DEMO_SPACE_KEY); /* We don't want content from the demo space to interfere this test case */
    }

    private List<String> createNumericLabelsUpToCount(int count)
    {
        Set<String> labels = new HashSet<String>();

        for (int i = 1; i <= count; ++i)
            labels.add(String.valueOf(i));

        return new ArrayList<String>(labels);
    }

    public void testShowTopOneHundredPopularLabelsByDefault()
    {
        for (int i = 1; i <= 101; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, createNumericLabelsUpToCount(i));

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "showTopOneHundredPopularLabelsByDefault", "{popular-labels}"
                )
        );

        for (int i = 1; i <= 100; ++i)
            assertEquals(
                    String.valueOf(i),
                    getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][" + i + "]/a")
            );

        assertElementNotPresentByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][101]");
    }

    public void testShowAllPopularLabelsByDefault()
    {
        for (int i = 1; i <= 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, createNumericLabelsUpToCount(i));

        createSpace(DEMO_SPACE_KEY, DEMO_SPACE_KEY, DEMO_SPACE_KEY);

        String dsLabelOne = "dslabelone";
        createPage(DEMO_SPACE_KEY, "1", StringUtils.EMPTY,
                Arrays.asList(
                        dsLabelOne
                )
        );

        String dsLabelTwo = "dslabeltwo";
        createPage(DEMO_SPACE_KEY, "2", StringUtils.EMPTY,
                Arrays.asList(
                        dsLabelTwo
                )
        );


        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testShowAllPopularLabelsByDefault", "{popular-labels}"
                )
        );

        for (int i = 1; i <= 10; ++i)
            assertEquals(
                    String.valueOf(i),
                    getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][" + i + "]/a")
            );

        assertEquals(
                dsLabelOne,
                getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][11]/a")
        );
        assertEquals(
                dsLabelTwo,
                getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][12]/a")
        );

        assertElementNotPresentByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][13]");
    }

    public void testPopularLabelsLimitedByCount()
    {
        for (int i = 1; i <= 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, createNumericLabelsUpToCount(i));

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testPopularLabelsLimitedByCount", "{popular-labels:count=5}"
                )
        );

        for (int i = 1; i <= 5; ++i)
            assertEquals(
                    String.valueOf(i),
                    getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][" + i + "]/a")
            );


        assertElementNotPresentByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][6]");
    }

    public void testPopularLabelsLimitedBySpace()
    {
        for (int i = 1; i <= 5; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, createNumericLabelsUpToCount(i));

        createSpace(DEMO_SPACE_KEY, DEMO_SPACE_KEY, DEMO_SPACE_KEY);


        String dsLabelOne = "dslabelone";
        createPage(DEMO_SPACE_KEY, "1", StringUtils.EMPTY,
                Arrays.asList(
                        dsLabelOne
                )
        );

        String dsLabelTwo = "dslabeltwo";
        createPage(DEMO_SPACE_KEY, "2", StringUtils.EMPTY,
                Arrays.asList(
                        dsLabelTwo
                )
        );

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testPopularLabelsLimitedBySpace", "{popular-labels:spaceKey=" + TEST_SPACE_KEY + "}"
                )
        );

        for (int i = 1; i <= 5; ++i)
            assertEquals(
                    String.valueOf(i),
                    getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][" + i + "]/a")
            );


        assertElementNotPresentByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[@class='popularlabel'][6]");
    }

    public void testPopularLabelsAsHeatMapOrderedAlphabetically()
    {
        SpaceHelper anotherSpace = getSpaceHelper("atst");

        anotherSpace.setName("Another Test Space");
        anotherSpace.setDescription("Another Test Space");

        assertTrue(anotherSpace.create());

        createPage(TEST_SPACE_KEY, "1", StringUtils.EMPTY, Arrays.asList("3"));
        createPage(TEST_SPACE_KEY, "2", StringUtils.EMPTY, Arrays.asList("3", "2"));
        createPage(TEST_SPACE_KEY, "3", StringUtils.EMPTY, Arrays.asList("3", "2", "1"));

        createPage(anotherSpace.getKey(), "4", StringUtils.EMPTY, Arrays.asList("4"));
        createPage(anotherSpace.getKey(), "5", StringUtils.EMPTY, Arrays.asList("4", "5"));

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testGlobalPopularLabelsAsHeatMapOrderedAlphabetically", "{popular-labels:style=heatmap}"
                )
        );

        assertEquals("1", getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li/a[@class='label']"));
        assertEquals("font-size:14px;", getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li", "style"));

        assertEquals("2", getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[2]/a[@class='label']"));
        assertEquals("font-size:20px;", getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[2]", "style"));

        assertEquals("3", getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[3]/a[@class='label']"));
        assertEquals("font-size:30px;", getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[3]", "style"));

        assertEquals("4", getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[4]/a[@class='label']"));
        assertEquals("font-size:20px;", getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[4]", "style"));

        assertEquals("5", getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[5]/a[@class='label']"));
        assertEquals("font-size:14px;", getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[5]", "style"));
    }

    public void testPopularLabelsAsListOrderedByRank()
    {
        SpaceHelper anotherSpace = getSpaceHelper("atst");

        anotherSpace.setName("Another Test Space");
        anotherSpace.setDescription("Another Test Space");

        assertTrue(anotherSpace.create());

        createPage(TEST_SPACE_KEY, "1", StringUtils.EMPTY, Arrays.asList("3"));
        createPage(TEST_SPACE_KEY, "2", StringUtils.EMPTY, Arrays.asList("3", "2"));
        createPage(TEST_SPACE_KEY, "3", StringUtils.EMPTY, Arrays.asList("3", "2", "1"));

        createPage(anotherSpace.getKey(), "4", StringUtils.EMPTY, Arrays.asList("4"));
        createPage(anotherSpace.getKey(), "5", StringUtils.EMPTY, Arrays.asList("4", "5"));

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testPopularLabelsAsListOrderedByRank", "{popular-labels}"
                )
        );

        assertEquals("3", getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li/a[@class='label']"));
        assertEquals("2", getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[2]/a[@class='label']"));
        assertEquals("4", getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[3]/a[@class='label']"));
        assertEquals("1", getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[4]/a[@class='label']"));
        assertEquals("5", getElementTextByXPath("//div[@class='wiki-content']/ul[contains(@class, 'popularlabels-list')]/li[5]/a[@class='label']"));
    }

    public void testRenderPopularLabelsAsHeatMap()
    {
        for (int i = 1; i <= 5; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, createNumericLabelsUpToCount(i));

        getIndexHelper().update();

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testRenderPopularLabelsAsHeatMap", "{popular-labels:spaceKey=" + TEST_SPACE_KEY + "|style=heatmap}"
                )
        );

        for (int i = 1; i <= 5; ++i)
            assertEquals(
                    String.valueOf(i),
                    getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[" + i + "]/a")
            );

        /* Make sure labels are rendered with different sizes */
        assertEquals(
                "font-size:30px;",
                getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li", "style")
        );
        assertEquals(
                "font-size:24px;",
                getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[2]", "style")
        );
        assertEquals(
                "font-size:18px;",
                getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[3]", "style")
        );
        assertEquals(
                "font-size:14px;",
                getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[4]", "style")
        );
        assertEquals(
                "font-size:14px;",
                getElementAttributeByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[5]", "style")
        );


        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[contains(@class, 'heatmap')]/ul/li[6]");
    }

    public void testLabelLinkPointToSpaceLabelIfSpaceKeyIsSpecified()
    {
        long testPageId = createPage(TEST_SPACE_KEY, "testLabelLinkPointToSpaceLabelsIfSpaceKeyIsSpecified", "{popular-labels:spaceKey=" + TEST_SPACE_KEY + "}", Arrays.asList("foo"));

        getIndexHelper().update();
        viewPageById(testPageId);

        clickLinkWithText("foo");

        assertTitleEquals("Labelled content - " + TEST_SPACE_TITLE + " - Confluence");
    }

    public void testLabelLinkPointToGlobalLabelIfSpaceKeyIsNotSpecified()
    {
        long testPageId = createPage(TEST_SPACE_KEY, "testLabelLinkPointToGlobalLabelsIfSpaceKeyIsNotSpecified", "{popular-labels}", Arrays.asList("foo"));

        getIndexHelper().update();
        viewPageById(testPageId);

        clickLinkWithText("foo");

        assertTitleEquals("Labelled content - Confluence");
    }
}
