package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

public class QueryParameters
{
    /**
     * Comma-delimited list of labels. Labels can be prefixed with + or - to indicate
     * required or excluded.
     */
    private final String labels;

    /**
     * Comma-delimited list of authors.
     */
    private final String authors;
    private final String spaceKeys;

    /**
     * Comma-delimited list of content types (e.g. {@link com.atlassian.confluence.pages.Page#CONTENT_TYPE}.
     * Types can be prefixed with + or - to indicate required or excluded.
     */
    private final String contentTypes;

    public QueryParameters(String labels, String authors, String contentTypes, String spaceKeys)
    {
        this.labels = labels;
        this.authors = authors;
        this.contentTypes = contentTypes;
        this.spaceKeys = spaceKeys;
    }

    public String getLabels()
    {
        return labels;
    }

    public String getAuthors()
    {
        return authors;
    }

    public String getContentTypes()
    {
        return contentTypes;
    }

    public String getSpaceKeys()
    {
        return spaceKeys;
    }
}
