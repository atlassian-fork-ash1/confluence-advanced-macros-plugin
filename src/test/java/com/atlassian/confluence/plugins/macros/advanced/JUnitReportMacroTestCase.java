package com.atlassian.confluence.plugins.macros.advanced;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.radeox.macro.parameter.MacroParameter;

import com.atlassian.confluence.renderer.radeox.macros.junit.JUnitTestCaseReport;
import com.atlassian.confluence.renderer.radeox.macros.junit.report.TestCaseReport;
import com.atlassian.confluence.renderer.radeox.macros.junit.report.TestSuite;

import junit.framework.TestCase;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JUnitReportMacroTestCase extends TestCase
{
	private File jUnitReportXml;
	
	private MacroParameter macroParameter;
	private Map<String, Object> contextMap;
	private String fakeOutput = "Execution Done";
	private String generateTemplateOutput = "Generate Done";

	@Override
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		jUnitReportXml = copyTestJunitReportXmlToTempIoDirectory();
		
		macroParameter = mock(MacroParameter.class);
		contextMap = new HashMap<String, Object>();
	}
	
	@Override
    protected void tearDown() throws Exception
    {
        try
        {
            jUnitReportXml.delete();
        }
        finally
        {
            super.tearDown();
        }
    }
	
	public void testGenerateJUnitReportUrl() throws IllegalArgumentException, IOException
	{
		JUnitReportMacro junitReportMacro = new JUnitReportMacro()
		{
			@Override
			protected void addEmoticonsAsSpecial() 
			{
				// Overwrite and do nothing
			}

			@Override
			protected Map<String, Object> getDefaultVelocityContext() 
			{
				return contextMap;
			}

			@Override
			protected String generateRenderedTemplate(Map<String, Object> contextMap) 
			{
				assertEquals("file:///" + jUnitReportXml.getPath(), contextMap.get("url"));
				
				JUnitTestCaseReport endReport = (JUnitTestCaseReport) contextMap.get("report");
				assertEquals("TestExampleMacro", endReport.getName());
				assertEquals("00:00.26", endReport.getTimeAsString());
				assertEquals(2, endReport.getTestCases().size());
				
				List<TestCaseReport> testCaseList = endReport.getTestCases();
				TestCaseReport report = testCaseList.get(0);
				assertEquals("testBasic", report.getName());
				assertEquals("00:00.2", report.getTimeAsString());
				
				report = testCaseList.get(1);
				assertEquals("testBasicFail", report.getName());
				assertEquals("00:00.2", report.getTimeAsString());
				
				assertEquals(TestSuite.REPORT_DETAIL_PER_FIXTURE, contextMap.get("reportDetail"));
				assertEquals("false", contextMap.get("debug").toString());
				
				return generateTemplateOutput;
			}
			
			@Override
			protected String doContentPreserver(String content) 
			{
				assertEquals("Generate Done", content);
				
				return fakeOutput;
			}
		};
		
		when(macroParameter.get("url")).thenReturn("file:///" + jUnitReportXml.getPath());
		
		assertEquals("Execution Done", junitReportMacro.getHtml(macroParameter));
	}
	
	public void testGetMacroName()
	{
	    JUnitReportMacro junitReportMacro = new JUnitReportMacro()
	    {
	        @Override
            protected void addEmoticonsAsSpecial() 
            {
                // Overwrite and do nothing
            }
	    };
	    
	    assertEquals("junitreport", junitReportMacro.getName());
	}
	
	public void testGetParameterDescription()
	{
	    String[] expectedDescription = new String[] {"?1: url", "?2: directory", "?3: reportdetail", "?4: debug"};
	    
	    JUnitReportMacro junitReportMacro = new JUnitReportMacro()
        {
            @Override
            protected void addEmoticonsAsSpecial() 
            {
                // Overwrite and do nothing
            }
        };
        
        String[] description = junitReportMacro.getParamDescription();
        
        assertEquals(expectedDescription[0], description[0]);
        assertEquals(expectedDescription[1], description[1]);
        assertEquals(expectedDescription[2], description[2]);
        assertEquals(expectedDescription[3], description[3]);
	}
	
	private File copyTestJunitReportXmlToTempIoDirectory() throws IOException
    {
        File tempFile = File.createTempFile("JUnitReportMacroTestCase", ".xml");
        
        FileUtils.copyURLToFile(
                getClass().getClassLoader().getResource("junitreport.xml"),
                tempFile
        );
        
        return tempFile;
    }
}
