package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.INCLUDES;
import static com.atlassian.confluence.search.service.ContentTypeEnum.BLOG;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PAGE;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BooleanQueryConverterTest
{
    private Set<SearchQuery> must;
    private Set<SearchQuery> should;
    private Set<SearchQuery> mustNot;
    @Mock private I18NBean i18n;

    @Before
    public void setUp() throws Exception
    {
        must = Sets.newHashSet();
        should = Sets.newHashSet();
        mustNot = Sets.newHashSet();
    }

    @Test
    public void testSingleLabelShould() throws Exception
    {
        should.add(new LabelQuery("foo"));

        assertThat(convertToString(), is("label = \"foo\""));
    }

    @Test
    public void testSingleLabelMust() throws Exception
    {
        must.add(new LabelQuery("foo"));

        assertThat(convertToString(), is("label = \"foo\""));
    }

    @Test
    public void testLabelNot() throws Exception
    {
        // It isn't permitted in the BooleanQuery to only provide a EXCLUDES expression, so pad it.
        must.add(new LabelQuery("foo"));
        mustNot.add(new LabelQuery("bar"));

        assertThat(convertToString(), is("label = \"foo\" and label != \"bar\""));
    }

    @Test
    public void testMultipleLabelMust() throws Exception
    {
        must.add(new LabelQuery("foo"));
        must.add(new LabelQuery("bar"));

        assertThat(convertToString(), is("label = \"foo\" and label = \"bar\""));
    }

    @Test
    public void testMultipleLabelNot() throws Exception
    {
        mustNot.add(new LabelQuery("foo"));
        mustNot.add(new LabelQuery("bar"));
        must.add(new LabelQuery("baz"));

        assertThat(convertToString(), is("label = \"baz\" and label not in (\"foo\",\"bar\")"));
    }

    @Test
    public void testMultipleLabelShould() throws Exception
    {
        should.add(new LabelQuery("foo"));
        should.add(new LabelQuery("bar"));

        assertThat(convertToString(), is("label in (\"foo\",\"bar\")"));
    }

    @Test
    public void testMixedLabelShouldMust() throws Exception
    {
        must.add(new LabelQuery("foo"));
        should.add(new LabelQuery("bar"));

        assertThat(convertToString(), is("label = \"foo\" and label = \"bar\""));
    }

    @Test
    public void testSingleContentTypeMust() throws Exception
    {
        must.add(new ContentTypeQuery(PAGE));

        assertThat(convertToString(), is("type = \"page\""));
    }

    @Test
    public void testMultipleContentTypeShould() throws Exception
    {
        should.add(new ContentTypeQuery(PAGE));
        should.add(new ContentTypeQuery(BLOG));

        QueryExpression actual = convertToExpression();
        assertThat(actual, instanceOf(SimpleQueryExpression.class));

        SimpleQueryExpression simple = (SimpleQueryExpression) actual;
        assertThat(simple.getKey(), is("type"));
        assertThat(simple.getInclusionOperator(), is(INCLUDES));
        assertThat(simple.getValues(), hasItems("page", "blogpost"));
    }

    @Test
    public void testSingleContentTypeMustNot() throws Exception
    {
        must.add(new LabelQuery("foo"));
        mustNot.add(new ContentTypeQuery(PAGE));

        assertThat(convertToString(), is("label = \"foo\" and type != \"page\""));
    }

    @Test
    public void testSingleContentTypeShould() throws Exception
    {
        should.add(new ContentTypeQuery(PAGE));

        assertThat(convertToString(), is("type = \"page\""));
    }

    private QueryExpression convertToExpression()
    {
        BooleanQuery query = new BooleanQuery(must, should, mustNot);

        return new BooleanQueryConverter(i18n).convertToExpression(query);
    }

    private String convertToString()
    {
        QueryExpression expression = convertToExpression();
        return expression.toQueryString();
    }
}
