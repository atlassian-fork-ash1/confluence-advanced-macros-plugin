package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.security.access.annotations.RequiresAnyConfluenceAccess;

/**
 * This action serves changes to the "get more" requests on the "sidebar" theme.
 */
@RequiresAnyConfluenceAccess
public class SideBarThemeChangesAction extends AbstractChangesAction
{
    protected Theme getTheme()
    {
        return Theme.sidebar;
    }
}
