package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.actions.RankedRankComparator;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PopularLabelsMacroTestCase extends TestCase
{
    @Mock private LabelManager labelManager;

    @Mock private VelocityHelperService velocityHelperService;

    private Page pageToRenderOn;

    private Map<String, String> macroParams;

    private PopularLabelsMacro popularLabelsMacro;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        pageToRenderOn = new Page();
        macroParams = new HashMap<String, String>();
        
        popularLabelsMacro = new PopularLabelsMacro();
        popularLabelsMacro.setLabelManager(labelManager);
        popularLabelsMacro.setVelocityHelperService(velocityHelperService);
    }

    @Override
    protected void tearDown() throws Exception
    {
        labelManager = null;
        velocityHelperService = null;
        super.tearDown();
    }

    public void testDefaultResultsCapIsOneHundred() throws MacroException
    {
        final Set<String> labels = Collections.emptySet();

        when(labelManager.getMostPopularLabelsWithRanks(eq(100), isA(RankedRankComparator.class))).thenReturn(labels);

        popularLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext());
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object o)
                            {
                                return labels.equals(((Map) o).get("labels"));
                            }
                        }
                )
        );
    }

    public void testCustomResultsCapRespected() throws MacroException
    {
        final Set labels = Collections.emptySet();

        when(labelManager.getMostPopularLabelsWithRanks(eq(50), isA(RankedRankComparator.class))).thenReturn(labels);

        macroParams.put("count", String.valueOf(50));

        popularLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext());
        verify(labelManager).getMostPopularLabelsWithRanks(eq(50), Matchers.<Comparator>anyObject());
    }

    public void testRenderPopularLabelsFromOtherSpace() throws MacroException
    {
        final Set<String> labels = Collections.emptySet();
        String spaceKey = "tst";

        when(labelManager.getMostPopularLabelsWithRanksInSpace(eq(spaceKey), anyInt(), isA(RankedRankComparator.class))).thenReturn(labels);

        macroParams.put("spaceKey", spaceKey);
        popularLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext());


        verify(labelManager).getMostPopularLabelsWithRanksInSpace(eq(spaceKey), anyInt(), Matchers.<Comparator>anyObject());
    }

    public void testStyleParamRespected() throws MacroException
    {

        final Set labels = Collections.emptySet();
        final String fakeOutput = "fakeOutput";
        String spaceKey = "tst";
        final String style = "style";

        when(labelManager.getMostPopularLabelsWithRanksInSpace(eq(spaceKey), anyInt(), isA(RankedRankComparator.class))).thenReturn(labels);

        macroParams.put("spaceKey", spaceKey);
        macroParams.put("style", style);

        popularLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object o)
                            {
                                return style.equals(((Map) o).get("style"));
                            }
                        }
                )
        );
    }

    public void testRendersBlock()
    {
        assertEquals(TokenType.BLOCK, popularLabelsMacro.getTokenType(null, null, null));
    }

    public void testHasNoBody()
    {
        assertFalse(popularLabelsMacro.hasBody());
    }

    public void testDoesNotRenderBody()
    {
        assertEquals(RenderMode.NO_RENDER, popularLabelsMacro.getBodyRenderMode());
    }
}
