package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Vector;

public class XmlRpcHelper
{
    private static final Logger LOG = LoggerFactory.getLogger(XmlRpcHelper.class);

    // Space Permissions
    public static final String VIEWSPACE_PERMISSION = "VIEWSPACE";
    public static final String COMMENT_PERMISSION = "COMMENT";
    public static final String CREATEEDIT_PAGE_PERMISSION = "EDITSPACE";
    public static final String ADMINISTER_SPACE_PERMISSION = "SETSPACEPERMISSIONS";
    public static final String REMOVE_PAGE_PERMISSION = "REMOVEPAGE";
    public static final String REMOVE_COMMENT_PERMISSION = "REMOVECOMMENT";
    public static final String REMOVE_BLOG_PERMISSION = "REMOVEBLOG";
    public static final String CREATE_ATTACHMENT_PERMISSION = "CREATEATTACHMENT";
    public static final String REMOVE_ATTACHMENT_PERMISSION = "REMOVEATTACHMENT";
    public static final String EDITBLOG_PERMISSION = "EDITBLOG";
    public static final String EXPORT_PAGE_PERMISSION = "EXPORTPAGE";
    public static final String EXPORT_SPACE_PERMISSION = "EXPORTSPACE";
    public static final String REMOVE_MAIL_PERMISSION = "REMOVEMAIL";
    public static final String SET_PAGE_PERMISSIONS_PERMISSION = "SETPAGEPERMISSIONS";

    // Global Permissions
    public static final String USE_CONFLUENCE_PERMISSION = "USECONFLUENCE";
    public static final String VIEW_USER_PROFILES_PERMISSION = "VIEWUSERPROFILES";
    public static final String SYSTEM_ADMINISTRATOR_PERMISSION = "SYSTEMADMINISTRATOR";
    public static final String CONFLUENCE_ADMINISTRATOR_PERMISSION = "ADMINISTRATECONFLUENCE";
    public static final String PERSONAL_SPACE_PERMISSION = "PERSONALSPACE";
    public static final String CREATE_SPACE_PERMISSION = "CREATESPACE";
    public static final String PROFILE_ATTACHMENT_PERMISSION = "PROFILEATTACHMENTS";

    protected ConfluenceWebTester confluenceWebTester;

    protected XmlRpcHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this.confluenceWebTester = confluenceWebTester;
    }

    public static XmlRpcHelper getInstance(ConfluenceWebTester webTester)
    {
        return new XmlRpcHelper(webTester);
    }

    public ConfluenceWebTester getConfluenceWebTester()
    {
        return confluenceWebTester;
    }

    public boolean enableAnonymousAccess()
    {
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, Boolean.toString(true) });
            result = (Boolean) xmlRpcClient.execute("confluence1.setEnableAnonymousAccess", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.setEnableAnonymousAccess XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.setEnableAnonymousAccess XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    public boolean disableAnonymousAccess()
    {
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, Boolean.toString(false) });
            result = (Boolean) xmlRpcClient.execute("confluence1.setEnableAnonymousAccess", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.setEnableAnonymousAccess XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.setEnableAnonymousAccess XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    public boolean grantAnonymousSpacePermission(String permission, String spaceKey)
    {
        // xmlRpcExecute("confluence1.addAnonymousPermissionToSpace",
        // makeParams(token, permission, spaceKey));
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, permission, spaceKey });
            result = (Boolean) xmlRpcClient.execute("confluence1.addAnonymousPermissionToSpace", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.addAnonymousPermissionToSpace XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.addAnonymousPermissionToSpace XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    public boolean grantGlobalPermission(Object permission, Object userName)
    {
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, permission, userName});
            result = (Boolean) xmlRpcClient.execute("confluence1.addGlobalPermission", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.addGlobalPermission XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.addGlobalPermission XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    public boolean grantSpacePermission(String permission, String user, String spaceKey)
    {
        // xmlRpcExecute("confluence1.addPermissionToSpace", makeParams(token,
        // permission, username, spaceKey));
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, permission, user, spaceKey });
            result = (Boolean) xmlRpcClient.execute("confluence1.addPermissionToSpace", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.addPermissionToSpace XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.addPermissionToSpace XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    public boolean grantViewSpacePermission(String user, String spaceKey)
    {
        // rpcGrantSpacePermission(token, SpacePermission.VIEWSPACE_PERMISSION,
        // entityName, spaceKey);
        // xmlRpcExecute("confluence1.addPermissionToSpace", makeParams(token,
        // permission, username, spaceKey));
        Boolean result = Boolean.FALSE;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            Vector params = makeParams(new Object[] { authenticationToken, VIEWSPACE_PERMISSION, user, spaceKey });
            result = (Boolean) xmlRpcClient.execute("confluence1.addPermissionToSpace", params);
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid XMLRPC URL.", mUrlE);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the confluence1.addPermissionToSpace XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the confluence1.addPermissionToSpace XMLRPC service.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return result.booleanValue();
    }

    /**
     * Returns the localised message text for the given key, retrieved from the
     * application via RPC. The message will be localised to the administrator's
     * locale. <p/> If the key is not found, the key itself will be returned, as
     * per
     * {@link com.atlassian.confluence.core.ConfluenceActionSupport#getText(String)}
     * .
     * 
     * @param messageKey
     *            the message key to internationalise.
     * @return a localised message in the administrator's locale
     * @throws IOException
     * @throws XmlRpcException
     * @see com.atlassian.confluence.test.rpc.FuncTestRpcHandler#getLocalisedText(String,
     *      String)
     * @see com.atlassian.confluence.core.ConfluenceActionSupport#getText(String)
     */
    public String getLocalisedText(String messageKey)
    {
        // return (String) xmlRpcExecute("functest.getLocalisedText",
        // makeParams(rpcGetAdminToken(), messageKey));
        String result = null;
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;

            authenticationToken = getConfluenceWebTester().loginToXmlRPcService();
            xmlRpcClient = getConfluenceWebTester().getXmlRpcClient();

            Vector params = new Vector();
            params.add(authenticationToken);
            params.add(messageKey);

            // TODO functest is not a recognized SOAP destination
            result = (String) xmlRpcClient.execute("functest.getLocalisedText", params);
        }
        catch (final XmlRpcException xre)
        {
            LOG.error("There's a problem invoking the functest.getLocalisedText XMLRPC service.", xre);
        }
        catch (final IOException ioe)
        {
            LOG.error("There's an IO problem when invoking the functest.getLocalisedText XMLRPC service.", ioe);
        }
        finally
        {
            getConfluenceWebTester().logoutFromXmlRpcService(authenticationToken);
        }

        return result;
    }

    private static Vector makeParams(Object[] params)
    {
        Vector paramsVector = new Vector();
        paramsVector.addAll(Arrays.asList(params));
        return paramsVector;
    }
}
