package it.com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.jsoup.JSoupTester;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.TestName;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Non-Webdriver acceptance tests that use XHTML, not wiki storage format and hence dispense with extending the heinous
 * AbstractConfluencePluginWebTestCaseBase.
 */
public class LabelledContentMacroXhtmlTest
{
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;

    @Before
    public void setUp() throws Exception
    {
        AcceptanceTestHelper helper = AcceptanceTestHelper.make();
        helper.setUp(getClass(), testName);
        rpc = helper.getRpc();
        rpc.logIn(User.ADMIN);
    }

    @Ignore(value = "Unignore when CQL version with currentContent is bumped in Core")
    @Test
    public void testCurrentContentInCql()
    {
        // The page with the macro will be the parent of the page with the label.
        Page parentPage = new Page(Space.TEST, "testCurrentContentInCql parent",
                "<ac:structured-macro ac:name=\"contentbylabel\" ac:schema-version=\"3\">" +
                    "<ac:parameter ac:name=\"cql\">label = 'foo' and parent = currentContent()</ac:parameter>" +
                "</ac:structured-macro>");
        Page createdParent = rpc.content.createPage(parentPage, ContentRepresentation.STORAGE);

        String childTitle = "testCurrentContentInCql child";
        Page childPage = new Page(Space.TEST, childTitle, "content", createdParent.getId());
        Page createdChild = rpc.content.createPage(childPage, ContentRepresentation.STORAGE);
        rpc.labels.addLabel("foo", createdChild);
        rpc.flushIndexQueue();

        // The child page should be present in the rendered macro.
        Content renderedParent = rpc.content.get(createdParent.getContentId(), ExpansionsParser.parse("body.view"));
        JSoupTester bodySoup = new JSoupTester(renderedParent.getBody().get(ContentRepresentation.VIEW).getValue());
        assertThat(bodySoup.findByCss(".content-by-label .details > a").first().text(), is(childTitle));
    }
}
