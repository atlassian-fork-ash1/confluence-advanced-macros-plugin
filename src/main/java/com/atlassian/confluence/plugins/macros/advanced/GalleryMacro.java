package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.NastyUtilitiesWhichWillBeMadeRedundant;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailInfo;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.pages.thumbnail.Thumbnails;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.attachments.RendererAttachmentManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.AttachmentComparator;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrMatcher;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Draw a thumbnail gallery from a page's image attachments. Will not work if thumbnails are globally disabled.
 */
public class GalleryMacro extends BaseMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(GalleryMacro.class);
    private static final int DEFAULT_COLUMNS = 4;

    private AttachmentManager attachmentManager;
    private ThumbnailManager thumbnailManager;
    private RendererAttachmentManager rendererAttachmentManager;
    private LinkResolver linkResolver;
    private ConfluenceActionSupport confluenceActionSupport;
    private static final String MACRO_NAME = "gallery";

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return execute(parameters, body, NastyUtilitiesWhichWillBeMadeRedundant.extractPageContext(conversionContext));
        }
        catch (MacroException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public void setAttachmentManager(AttachmentManager attachmentManager)
    {
        this.attachmentManager = attachmentManager;
    }

    public void setRendererAttachmentManager(RendererAttachmentManager rendererAttachmentManager)
    {
        this.rendererAttachmentManager = rendererAttachmentManager;
    }

    public void setThumbnailManager(ThumbnailManager thumbnailManager)
    {
        this.thumbnailManager = thumbnailManager;
    }

    public void setLinkResolver(LinkResolver linkResolver)
    {
        this.linkResolver = linkResolver;
    }

    public String getName()
    {
        return MACRO_NAME;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        @SuppressWarnings("unchecked")
        Map<String, String> typeSafeMacroParams = (Map<String, String>) params;

        if (!isThumbnailSupported())
        {
            return new StringBuffer("<p><span class=\"error\">")
                    .append(getConfluenceActionSupport().getText("gallery.error.thumbnails-not-supported"))
                    .append("</span></p>").toString();
        }

        Integer galleryId = nextGalleryId(renderContext);
        String title = typeSafeMacroParams.get("title");
        Thumbnails thumbnails = findThumbnails(typeSafeMacroParams, renderContext);
        boolean slideshow = renderContext.getOutputType().equals(RenderContextOutputType.DISPLAY);

        String template = getTemplate(null);
        VelocityContext contextMap = createVelocityContext(galleryId, title, thumbnails, slideshow);

        try
        {
            return getRenderedTemplateWithoutSwallowingErrors(template, contextMap);
        }
        catch (ResourceNotFoundException e)
        {
            return new StringBuffer("<p><span class='error'>")
                    .append(
                            getConfluenceActionSupport().getText(
                                    "gallery.error.unable-to-find-render-template",
                                    new String[] { StringEscapeUtils.escapeHtml(String.valueOf(typeSafeMacroParams.get("theme"))) }
                            )
                    ).append("</span></p>")
                    .toString();
        }
        catch (Exception e)
        {
            log.error("Error while trying to draw the image gallery", e);
            return new StringBuffer("<p><span class='error'>")
                    .append(
                            getConfluenceActionSupport().getText(
                                    "gallery.error.unable-to-render",
                                    new String[] { StringEscapeUtils.escapeHtml(e.toString()) }
                            )
                    ).append("</span></p>")
                    .toString();
        }
    }

	protected boolean isThumbnailSupported() {
		return ThumbnailInfo.systemSupportsThumbnailing();
	}

///CLOVER:OFF
	protected String getRenderedTemplateWithoutSwallowingErrors(
			String template, VelocityContext contextMap) throws Exception {
		return VelocityUtils.getRenderedTemplateWithoutSwallowingErrors(template, contextMap);
	}
///CLOVER:ON
	
    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        // This doesn't really usually have a body, but the example content uses an end tag, and some other existing
        // uses might too, so we have to say it has a body to avoid duplicating the gallery.
        //
        // The atlassian-plugin.xml specifies a body-type of NONE, so that the xhtml compatibility version does not have
        // this issue and entry of the macro as wiki text also works fine if the closing {gallery} is supplied or not.
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    private VelocityContext createVelocityContext(Integer galleryId, Object title, Thumbnails thumbnails,
                                                  boolean slideshow)
    {
        VelocityContext contextMap = newVelocityContext();
        contextMap.put("galleryId", galleryId);
        contextMap.put("galleryTitle", title);
        contextMap.put("thumbnails", thumbnails);
        contextMap.put("slideshow", slideshow);
        return contextMap;
    }

    private Thumbnails findThumbnails(Map<String, String> params, RenderContext renderContext)
    {
        int columns = getColumns(params.get("columns"));
        String sort = params.get("sort");
        boolean reverseSort = BooleanUtils.toBoolean(params.get("reverse"));
		FilterParams filterParams = new FilterParams(
				splitList(params.get("include")),
				splitList(params.get("exclude")),
				splitList(params.get("includeLabel")),
				splitList(params.get("excludeLabel")));
        
        String[] pages = splitList(params.get("page"));

        List<Attachment> attachments = new ArrayList<Attachment>();
        if (renderContext instanceof PageContext)
        {
            PageContext context = (PageContext) renderContext;
            if (pages.length > 0)
            {
                for (String page : pages)
                {
                    Link link = linkResolver.createLink(context, page);
                    if (link != null && link instanceof PageLink)
                    {
                        ContentEntityObject content = ((PageLink) link).getDestinationContent();
                        addContentAttachmentsToList(content, attachments);
                    }
                }
            }
            else
            {
                ContentEntityObject content = context.getEntity();
                addContentAttachmentsToList(content, attachments);
            }
        }

        attachments = filterAttachments(filterParams, attachments);
        sortAttachments(sort, reverseSort, attachments);
        return createThumbnails(columns, attachments);
    }
    
    private static class FilterParams
    {
    	final Set<String> include;
    	final Set<String> exclude;
    	final Set<String> includeLabels;
    	final Set<String> excludeLabels;
    	FilterParams(String[] include, String[] exclude, String[] includeLabels, String[] excludeLabels)
    	{
    		this.include = ImmutableSet.copyOf(include);
    		this.exclude =  ImmutableSet.copyOf(exclude);
    		this.includeLabels =  ImmutableSet.copyOf(includeLabels);
    		this.excludeLabels =  ImmutableSet.copyOf(excludeLabels);
    	}
    }

	protected Thumbnails createThumbnails(int columns,
			List<Attachment> attachments) {
		return new Thumbnails(attachments, rendererAttachmentManager, columns, attachmentManager, thumbnailManager);
	}

    private String getTemplate(String theme)
    {
        if (StringUtils.isBlank(theme))
            theme = "default";

        return "/com/atlassian/confluence/plugins/macros/advanced/gallery-" + theme + ".vm";
    }

    private int getColumns(String columns)
    {
        int colCount = 0;
        if (columns != null)
            try
            {
                colCount = Integer.parseInt(columns);
            }
            catch (NumberFormatException e)
            {
                // don't care
            }

        return (colCount == 0) ? DEFAULT_COLUMNS : colCount;
    }

    private String[] splitList(String commaDelimitedList)
    {
        return new StrTokenizer(commaDelimitedList, StrMatcher.commaMatcher(), StrMatcher.quoteMatcher())
            .setTrimmerMatcher(StrMatcher.trimMatcher())
            .getTokenArray();
    }

    @SuppressWarnings({"unchecked"})
    private void addContentAttachmentsToList(ContentEntityObject content, List<Attachment> attachments)
    {
        if (content != null)
            attachments.addAll(attachmentManager.getLatestVersionsOfAttachments(content));
    }

    private List<Attachment> filterAttachments(final FilterParams filterParams, List<Attachment> attachments)
    {
    	Predicate<Attachment> filter = Predicates.alwaysTrue();
    	Function<Attachment, String> attachmentToNameFn = new Function<Attachment, String>()
    	{
    		public String apply(Attachment input) 
    		{
				return input.getFileName();
			}
    	};
    	
    	if(!filterParams.include.isEmpty())
    	{
    		filter = Predicates.and(filter, Predicates.compose(Predicates.in(filterParams.include), attachmentToNameFn));
    	}

    	if(!filterParams.exclude.isEmpty())
    	{
    		filter = Predicates.and(filter, Predicates.not(Predicates.compose(Predicates.in(filterParams.exclude), attachmentToNameFn)));
    	}
    	
    	if(!filterParams.excludeLabels.isEmpty())
    	{
    		filter = Predicates.and(filter, Predicates.not(new Predicate<Attachment>(){
    			@Override
    			public boolean apply(Attachment input) {

    				for(Label label : input.getLabels())
    				{
    					if(filterParams.excludeLabels.contains(label.getName()))
    						return true;
    				}
    				return false;
    			}
    		}));
    	}
    	
    	if(!filterParams.includeLabels.isEmpty())
    	{
    		filter = Predicates.and(filter, new Predicate<Attachment>(){
    			@Override
    			public boolean apply(Attachment input) 
    			{
    				for(Label label : input.getLabels())
    				{
    					if(filterParams.includeLabels.contains(label.getName()))
    						return true;
    				}
    				return false;
    			}
    		});
    	};
    	
    	return Lists.newArrayList(Iterables.filter(attachments, filter));
    }

    @SuppressWarnings({"unchecked"})
    private void sortAttachments(String sort, boolean reverseSort, List attachments)
    {
        if (StringUtils.isBlank(sort))
        {
            return;
        }

        Comparator comparator = new GalleryAttachmentComparator(sort, reverseSort);
        Collections.sort(attachments, comparator);
    }

    private Integer nextGalleryId(RenderContext renderContext)
    {
        Integer galleryId = (Integer) renderContext.getParam("nextGalleryId");
        if (galleryId == null)
        {
            galleryId = 0;
        }
        renderContext.addParam("nextGalleryId", galleryId + 1);
        return galleryId;
    }

    public ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }
    
    ///CLOVER:OFF
    protected VelocityContext newVelocityContext()
    {
        return new VelocityContext(MacroUtils.defaultVelocityContext());
    }
    ///CLOVER:OFF

    private class GalleryAttachmentComparator extends AttachmentComparator
    {
        private String sortBy;
        private boolean reverse;

        public GalleryAttachmentComparator(String sortBy, boolean reverse)
        {
            super(sortBy, reverse);
            this.sortBy = sortBy;
            this.reverse = reverse;
        }

        @Override
        public int compare(Object o1, Object o2)
        {
            if (StringUtils.equals(sortBy, COMMENT_SORT))
            {
                Attachment anAttachment = (Attachment) o1;
                Attachment anotherAttachment = (Attachment) o2;

                int compareResult = Collator.getInstance().compare(
                        StringUtils.defaultString(anAttachment.getComment()),
                        StringUtils.defaultString(anotherAttachment.getComment())
                );

                return reverse ? -compareResult : compareResult;
            }
            else
            {
                return super.compare(o1, o2);
            }
        }
    }
}
