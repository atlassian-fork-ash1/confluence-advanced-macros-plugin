package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.WikiRendererContextKeys;
import com.atlassian.confluence.renderer.radeox.macros.AbstractHtmlGeneratingMacro;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.opensymphony.webwork.ServletActionContext;
import org.radeox.macro.parameter.MacroParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Another SnipSnap compatibility macro. We only support {index:AtoZ} -style indexes
 * at the moment, but it should be pretty easy to add other ones - just change the
 * .vm file that is doing the rendering.
 */
public class IndexMacro extends AbstractHtmlGeneratingMacro
{
    private static final Logger log = LoggerFactory.getLogger(IndexMacro.class);

    private static final String MACRO_NAME = "index";

    private String[] myParamDescription = new String[] { "1: ignored" };
    private SpaceManager spaceManager;
    private PermissionManager permissionManager;
    private PageManager pageManager;
    private SettingsManager settingsManager;

    public String getName()
    {
        return MACRO_NAME;
    }

    public String[] getParamDescription()
    {
        return myParamDescription;
    }

    public String getHtml(MacroParameter macroParameter) throws IllegalArgumentException, IOException
    {
        Map contextParams = macroParameter.getContext().getParameters();

        Map<String, Object> contextMap = getDefaultVelocityContext();

        List<Page> pages = fetchPages(contextParams);
        contextMap.put("pages", new AlphabeticPageListing(pages));

        // use request contextPath if it exists, otherwise (as in exports) use the configured base URL
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request != null)
        {
            contextMap.put("baseurl", request.getContextPath());
        }
        else
        {
            contextMap.put("baseurl", settingsManager.getGlobalSettings().getBaseUrl());
        }
        contextMap.put("thisMacro",this);

        try
        {
            return getVelocityRenderedTemplate(contextMap);
        }
        catch (Exception e)
        {
            log.error("Error while trying to assemble the IndexMacro result!", e);
            throw new IOException(e.getMessage());
        }
    }

    ///CLOVER:OFF
    protected Map<String, Object> getDefaultVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }

    protected String getVelocityRenderedTemplate(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/macros/advanced/alphaindex.vm", contextMap);
    }
    ///CLOVER:ON

    @HtmlSafe
    public String getHtmlSafeExcerpt(Page page)
    {
        return GeneralUtil.htmlEncode(page.getExcerpt());
    }

    private List<Page> fetchPages(Map contextParams)
    {
        String currentSpaceKey = ((PageContext) contextParams.get(WikiRendererContextKeys.RENDER_CONTEXT)).getSpaceKey();
        return permissionManager.getPermittedEntities(
                AuthenticatedUserThreadLocal.getUser(),
                Permission.VIEW,
                pageManager.getPages(spaceManager.getSpace(currentSpaceKey), true)
        );
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public static class AlphabeticPageListing
    {
        private Map<String, TreeSet<Page>> pageMap;
        private static final String[] keys =
                                { "0-9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
                                  "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
                                  "Z", "!@#$" };
        private List<String> keyList = Arrays.asList(keys);

        public AlphabeticPageListing(List<Page> pages)
        {
            pageMap = new HashMap<String, TreeSet<Page>>();
            for (Page page : pages)
            {
                char c = Character.toUpperCase(page.getTitle().charAt(0));
                if (Character.isDigit(c))
                    addToMap("0-9", page);
                else if (c >= 'A' && c <= 'Z')
                    addToMap(Character.toString(c), page);
                else
                    addToMap("@", page);
            }
        }

        public List<String> getKeys()
        {
            return keyList;
        }

        public int getPageCount(String key)
        {
            if (!pageMap.containsKey(key))
            {
                return 0;
            }

            return pageMap.get(key).size();
        }

        public Set getPages(String key)
        {
            if (!pageMap.containsKey(key))
            {
                return new TreeSet();
            }

            return pageMap.get(key);
        }

        private void addToMap(String key, Page page)
        {
            if (!pageMap.containsKey(key))
            {
                pageMap.put(key, new TreeSet<Page>());
            }

            pageMap.get(key).add(page);
        }
    }
}
