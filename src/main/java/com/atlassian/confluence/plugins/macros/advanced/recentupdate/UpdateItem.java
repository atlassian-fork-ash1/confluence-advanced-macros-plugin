package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

/**
 * Represents a particular update.
 */
public interface UpdateItem
{
    /**
     * @return the person performing the update
     */
    @HtmlSafe Updater getUpdater();

    /**
     * @return title of what's been updated
     */
    @HtmlSafe String getUpdateTargetTitle();

    /**
     * Allows clients to supply a body for the update item (say a thumbnail of new attachment or the excerpt of a comment).
     * @return the body of the update item
     */
    @HtmlSafe String getBody();

    /**
     * @return formatted update date
     */
    @HtmlSafe String getFormattedDate();

    /**
     * @return the css class that defines an icon for this update.
     */
    @HtmlSafe String getIconClass();

    /**
     * An internationalised sentence that describes the update and when it occurred (e.g. created 2 mins ago, edited yesterday 10:43am).
     *
     * @return a description of the update (e.g. created, edited, attached) and the date
     */
    @HtmlSafe String getDescriptionAndDate();

    /**
     * An internationalised sentence that describes the update and who it was by (e.g. created by Dave, edited by Charles).
     *
     * @return a description of the update (e.g. created, edited, attached) and the author
     */
    @HtmlSafe String getDescriptionAndAuthor();
}
