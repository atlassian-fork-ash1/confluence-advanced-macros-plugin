package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

/**
 * Represents a grouping based on updater.
 */
public class GenericGrouping extends AbstractGrouping
{
    public GenericGrouping(Updater updater)
    {
        super(updater);
    }

    /**
     * @return true if this grouping is for the specified updater, false otherwise. 
     */
    private boolean forUpdater(Updater user)
    {
        if (updater == null)
            return user == null;

        return updater.equals(user);
    }

    public boolean canAdd(UpdateItem updateItem)
    {
        return forUpdater(updateItem.getUpdater());
    }
}
