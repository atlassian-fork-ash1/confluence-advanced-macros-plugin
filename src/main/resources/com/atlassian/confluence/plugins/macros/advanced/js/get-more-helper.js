(function ($) {
    AJS.moreLinkClickHandler = function (e) {
        var $this = $(this),
            nextPageUrl = $this.attr("href"),
            $moreLinkContainer = $this.closest(".more-link-container");

        if (!$moreLinkContainer.length) {
            throw new Error("Could not find more link container when one was expected.");
        }

        $moreLinkContainer.addClass("loading");
        $.get(nextPageUrl, function (data) {
            var $data = $(data).wrap("<div/>").parent();
            var $updateItems = $data.children("ul").children("li"); /* there could be nested LI's - we only want top level ones */
            $moreLinkContainer.closest(".results-container").children("ul").append($updateItems);
            var $updatedMoreLinkContainer = $data.find(".more-link-container");

            if ($updatedMoreLinkContainer.length === 0) {
                $moreLinkContainer.remove();
            } else {
                $moreLinkContainer.replaceWith($updatedMoreLinkContainer);
            }
            if (AJS.PageGadget && AJS.PageGadget.contentsUpdated) {AJS.PageGadget.contentsUpdated();}
        });

        return AJS.stopEvent(e);
    };
})(AJS.$);


