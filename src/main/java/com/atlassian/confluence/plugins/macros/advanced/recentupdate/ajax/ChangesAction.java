package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.security.access.annotations.RequiresAnyConfluenceAccess;

/**
 * Action responsible for dispatching to appropriate changes action.
 *
 * @see SocialThemeChangesAction
 * @see ConciseThemeChangesAction
 */
@RequiresAnyConfluenceAccess
public class ChangesAction extends ConfluenceActionSupport
{
    private String theme;

    @Override
    public void validate()
    {
        try
        {
            Theme.valueOf(theme);
        }
        catch (IllegalArgumentException e)
        {
            // CONF-35189 not reflecting user input in the error message
            addActionError("The requested theme is not supported.");
        }

        super.validate();
    }

    @Override
    public String execute() throws Exception
    {
        return theme;
    }

    public void setTheme(String theme)
    {
        this.theme = theme;
    }
}
