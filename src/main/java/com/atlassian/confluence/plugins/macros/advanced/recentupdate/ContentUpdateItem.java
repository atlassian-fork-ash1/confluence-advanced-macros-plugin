package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.importexport.impl.ExportUtils;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentUpdateItem extends AbstractUpdateItem
{
    private static final Logger log = LoggerFactory.getLogger(ContentUpdateItem.class);

    public ContentUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, String iconClass)
    {
        super(searchResult, dateFormatter, i18n, iconClass);
    }

    @HtmlSafe
    public String getBody()
    {
//        return searchResult.getExtraFields().get(DocumentFieldName.VERSION_COMMENT);
        return null;
    }

    @Override
    protected String getUpdateTargetToolTip()
    {
        return getSpaceName();
    }

    @HtmlSafe
    public String getChangesLink()
    {
        final String latestVersionId = searchResult.getExtraFields().get(DocumentFieldName.LATEST_VERSION_ID);
        final String versionString = searchResult.getExtraFields().get(DocumentFieldName.CONTENT_VERSION);

        if (StringUtils.isBlank(latestVersionId) || StringUtils.isBlank(versionString))
            return null;

        int version;
        try
        {
            version = Integer.parseInt(versionString);
        }
        catch (NumberFormatException e)
        {
            log.debug(versionString + " could not be parsed into an integer.");
            return null;
        }

        if (version <= 1)
            return null;

        //diffpagesbyversion.action can only take an AbstractPage content type, else it throws a 404
        if (isPageContentType(searchResult))
        {
            return String.format("<a class=\"changes-link\" href=\"%s/pages/diffpagesbyversion.action?pageId=%s&selectedPageVersions=%s&selectedPageVersions=%s\">%s</a>",
                RequestCacheThreadLocal.getContextPath(),
                latestVersionId,
                version,
                version - 1,
                i18n.getText("update.item.changes"));
        }
        else
        {
            return null;
        }

    }

    private boolean isPageContentType(SearchResult searchResult)
    {
        return Page.CONTENT_TYPE.equals(searchResult.getType())
                || BlogPost.CONTENT_TYPE.equals(searchResult.getType());
    }

    public String getDescriptionAndDateKey()
    {
        String i18nKey;

        int version = UpdateItemUtils.getContentVersion(searchResult);

        if (version > 0)
            i18nKey = version == 1 ? "update.item.desc.created" : "update.item.desc.updated";
        else
            i18nKey = "update.item.desc.generic";

        return i18nKey;
    }

    protected String getDescriptionAndAuthorKey()
    {
        String i18nKey;

        int version = UpdateItemUtils.getContentVersion(searchResult);

        if (version > 0)
            i18nKey = version == 1 ? "update.item.desc.author.created" : "update.item.desc.author.updated";
        else
            i18nKey = "update.item.desc.author.generic";

        return i18nKey;
    }

    @HtmlSafe
    @Override
    public String getLinkedUpdateTargetForHtmlExport()
    {
        if (searchResult.getSpaceName().equals(getUpdateTargetTitle())) {
            // this is the page representing the entire space
            return String.format("<a href=\"index.html\" title=\"%s\">%s</a>", getUpdateTargetToolTip(), getUpdateTargetTitle());
        } else {
            return String.format("<a href=\"%s\" title=\"%s\">%s</a>", ExportUtils.getTitleAsHref(searchResult),
                    getUpdateTargetToolTip(), getUpdateTargetTitle());
        }
    }

}
