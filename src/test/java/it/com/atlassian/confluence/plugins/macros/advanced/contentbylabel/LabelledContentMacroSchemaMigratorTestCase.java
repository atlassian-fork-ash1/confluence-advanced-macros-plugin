package it.com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.jsoup.JSoupTester;
import com.atlassian.confluence.it.plugin.Plugin;
import com.atlassian.confluence.it.plugin.SimplePlugin;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TestName;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.EDITOR;
import static com.atlassian.confluence.api.model.content.ContentRepresentation.STORAGE;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests that version 1 contentbylabel macro instances are upgraded to version 3, with a cql parameter and excerptType.
 */
public class LabelledContentMacroSchemaMigratorTestCase
{
    private AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;

    @Before
    public void setUp() throws Exception
    {
        helper.setUp(getClass(), testName);
        rpc = helper.getRpc();
        rpc.logIn(User.ADMIN);
    }

    @Test
    public void testMigrationDuringStorage() throws Exception
    {
        Page page = new Page(Space.TEST, "LabelledContentMacro Migration",
                "<ac:structured-macro ac:name=\"contentbylabel\">" +
                        "<ac:parameter ac:name=\"label\">foo</ac:parameter>" +
                        "<ac:parameter ac:name=\"excerpt\">true</ac:parameter>" +
                "</ac:structured-macro>");

        createContentAndValidateMacro(page);
    }

    @Test
    public void testMigrationDuringStorageFromVersion2() throws Exception
    {
        Page page = new Page(Space.TEST, "LabelledContentMacro Migration",
                "<ac:structured-macro ac:name=\"contentbylabel\" ac:schema-version=\"2\">" +
                        "<ac:parameter ac:name=\"cql\">label = &quot;label&quot;</ac:parameter>\"" +
                        "<ac:parameter ac:name=\"excerpt\">true</ac:parameter>" +
                "</ac:structured-macro>");

        createContentAndValidateMacro(page);
    }

    private void createContentAndValidateMacro(Page page)
    {
        Page created = rpc.content.createPage(page, STORAGE);

        // Getting the page in storage format should get the migrated, stored format.
        Content content = rpc.content.get(created.getContentId(), ExpansionsParser.parseSingle("body.storage"));
        String body = content.getBody().get(STORAGE).getValue();
        JSoupTester soup = new JSoupTester(body);
        Element macroElement = soup.global().byAttribute("ac:name", "contentbylabel").first();

        assertThat(macroElement.attr("ac:schema-version"), is("3"));
        assertThat(macroElement.getElementsByAttributeValue("ac:name", "cql").isEmpty(), is(false));
        assertThat(macroElement.getElementsByAttributeValue("ac:name", "excerptType").text(), is("simple"));
    }

    @Test
    public void testMigrationDuringEdit() throws Exception
    {
        Plugin plugin = new SimplePlugin("confluence.macros.advanced");
        String moduleKey = "contentbylabel-migrator";
        // Disable the migrator so that the storage format is unchanged
        rpc.getPluginHelper().disablePluginModule(plugin, moduleKey);

        Page page = new Page(Space.TEST, "LabelledContentMacro Migration",
                "<ac:structured-macro ac:name=\"contentbylabel\">" +
                        "<ac:parameter ac:name=\"label\">foo</ac:parameter>" +
                        "<ac:parameter ac:name=\"excerpt\">true</ac:parameter>" +
                "</ac:structured-macro>");

        Page created = rpc.content.createPage(page, STORAGE);

        rpc.addLabel("foo", created);

        rpc.getPluginHelper().enablePluginModule(plugin, moduleKey);

        // Getting the page in view format should get the migrated view format.
        Content content = rpc.content.get(created.getContentId(), ExpansionsParser.parseSingle("body.editor"));
        String body = content.getBody().get(EDITOR).getValue();
        JSoupTester soup = new JSoupTester(body);
        Element macroElement = soup.global().byAttribute("class", "editor-inline-macro").first();

        assertThat(macroElement.attr("data-macro-schema-version"), is("3"));
        assertThat(macroElement.attr("data-macro-parameters"), containsString("cql=label \\= \"foo\""));
        assertThat(macroElement.attr("data-macro-parameters"), containsString("excerptType=simple"));
    }

    @Test
    public void testMigrationOfMultipleSpaceCategoriesDuringStorage() throws Exception
    {
        Page page = new Page(Space.TEST, "LabelledContentMacro Migration",
                "<ac:structured-macro ac:name=\"contentbylabel\">" +
                        "<ac:parameter ac:name=\"label\">foo</ac:parameter>" +
                        "<ac:parameter ac:name=\"space\">@global,@favourite</ac:parameter>" +
                "</ac:structured-macro>");
        Page created = rpc.content.createPage(page, STORAGE);

        // Getting the page in storage format should get the migrated, stored format.
        Content content = rpc.content.get(created.getContentId(), ExpansionsParser.parseSingle("body.storage"));
        String body = content.getBody().get(STORAGE).getValue();
        JSoupTester soup = new JSoupTester(body);
        Element macroElement = soup.global().byAttribute("ac:name", "contentbylabel").first();

        // The error reported in CRA-520 shouldn't occur during migration; it was found when testing via wiki-markup.
        assertThat(macroElement.getElementsByAttributeValue("ac:name", "cql").text(), containsString("space.type in ("));
    }
}
