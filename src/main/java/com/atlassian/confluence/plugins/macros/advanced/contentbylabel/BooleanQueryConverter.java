package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.macro.query.params.BooleanQueryFactoryParameter;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.service.SpaceCategoryEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.AllQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.query.SpaceCategoryQuery;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator.AND;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator.OR;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.INCLUDES;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.EXCLUDES;

/**
 * The {@link BooleanQuery} instances created by {@link BooleanQueryFactoryParameter}s include some useful business
 * logic that we should reuse until we rewrite them as "QueryExpressionFactoryParameters" at some indeterminate point
 * in the future.
 *
 * Methods on this class convert incoming legacy queries into simple and composite QueryExpressions.
 */
public class BooleanQueryConverter
{
    private final I18NBean i18n;

    protected BooleanQueryConverter(I18NBean i18n)
    {
        this.i18n = i18n;
    }

    public QueryExpression convertToExpression(BooleanQuery query)
    {
        CompositeQueryExpression.Builder builder = CompositeQueryExpression.builder(AND);

        builder.add(getExpression(AND, INCLUDES, query.getMustQueries()));
        builder.add(getExpression(OR, INCLUDES, query.getShouldQueries()));
        builder.add(getExpression(AND, EXCLUDES, query.getMustNotQueries()));

        return builder.build();
    }

    private QueryExpression getExpression(BooleanOperator booleanOperator,
                                                 SimpleQueryExpression.InclusionOperator inclusionOperator,
                                                 Set<SearchQuery> queries)
    {
        if (queries.isEmpty())
            return null;

        // Should queries can be converted into "foo in (bar,baz)" expressions
        Map<String, List<String>> queryMap = buildQueryMap(queries);

        CompositeQueryExpression.Builder builder = CompositeQueryExpression.builder(booleanOperator);

        for (Map.Entry<String, List<String>> entry : queryMap.entrySet())
        {
            String key = entry.getKey();
            List<String> values = entry.getValue();
            QueryExpression expression;
            if (booleanOperator == AND && inclusionOperator == INCLUDES)
            {
                // AND/INCLUDES must be one simple expression per value
                CompositeQueryExpression.Builder subBuilder = CompositeQueryExpression.builder(AND);
                for (String value : values)
                {
                    subBuilder.add(SimpleQueryExpression.of(key, INCLUDES, value));
                }
                expression = subBuilder.build();
            }
            else
            {
                // other operators can be bundled into one in(values) expression.
                expression = SimpleQueryExpression.of(key, inclusionOperator, values);
            }
            builder.add(expression);
        }
        return builder.build();
    }

    private Map<String, List<String>> buildQueryMap(Set<SearchQuery> queries)
    {
        Map<String, List<String>> queryMap = Maps.newHashMap();

        for (SearchQuery searchQuery : queries)
        {
            if (searchQuery instanceof LabelQuery)
            {
                String labelAsString = getLabelValue((LabelQuery) searchQuery);
                addQueryToMap(queryMap, "label", labelAsString);
            }
            else if (searchQuery instanceof ContentTypeQuery)
            {
                ContentTypeQuery typeQuery = (ContentTypeQuery) searchQuery;
                Set<ContentTypeEnum> contentTypes = typeQuery.getContentTypes();
                for (ContentTypeEnum contentType : contentTypes)
                {
                    addQueryToMap(queryMap, "type", contentType.getRepresentation());
                }
            }
            else if (searchQuery instanceof InSpaceQuery)
            {
                // InSpaceQuery instances are constructed with singleton String lists.
                String spaceKey = (String) searchQuery.getParameters().get(0);
                addQueryToMap(queryMap, "space", spaceKey);
            }
            else if (searchQuery instanceof SpaceCategoryQuery)
            {
                SpaceCategoryQuery categoryQuery = (SpaceCategoryQuery) searchQuery;
                Set<SpaceCategoryEnum> spaceCategories = categoryQuery.getSpaceCategories();
                for (SpaceCategoryEnum spaceCategory : spaceCategories)
                {
                    String spaceType = getSpaceType(spaceCategory);
                    if (spaceType != null)
                        addQueryToMap(queryMap, "space.type", spaceType);
                }
            }
            else if (!(searchQuery instanceof AllQuery))
            {
                // Just ignore AllQuery for now - they're just filler to avoid Lucene issues.
                // Any other unknown query class means that something has gone wrong - throw.
                throw new IllegalArgumentException("Unknown query class: " + searchQuery.getClass());
            }
        }
        return queryMap;
    }

    private String getLabelValue(LabelQuery labelQuery)
    {
        // Strip 'global:'
        ParsedLabelName parsedLabel = (ParsedLabelName) labelQuery.getParameters().get(0);
        return parsedLabel.getPrefix().equals("global") ? parsedLabel.getName() : labelQuery.getLabelAsString();
    }

    private String getSpaceType(SpaceCategoryEnum category)
    {
        switch (category)
        {
            case ALL:        return null; // no space filtering
            case GLOBAL:     return "global";
            case PERSONAL:   return "personal";
            case FAVOURITES: return "favourite";
        }

        // The user did something really special to get here. Let them know that.
        throw new IllegalArgumentException(i18n.getText("contentbylabel.error.invalid-space", new String[]{category.getRepresentation()}));
    }

    private void addQueryToMap(Map<String, List<String>> map, String key, String value)
    {
        List<String> queries = map.get(key);
        if (queries == null)
        {
            queries = Lists.newArrayList();
            map.put(key, queries);
        }

        queries.add(value);
    }
}
