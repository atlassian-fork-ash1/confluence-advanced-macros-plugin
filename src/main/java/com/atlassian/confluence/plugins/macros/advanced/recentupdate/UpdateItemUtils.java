package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateItemUtils
{
    private static final Logger log = LoggerFactory.getLogger(UpdateItemUtils.class);

    public static int getContentVersion(SearchResult searchResult)
    {
        final String contentVersion = searchResult.getExtraFields().get(DocumentFieldName.CONTENT_VERSION);
        int version = -1;
        if (StringUtils.isNotBlank(contentVersion))
        {
            try
            {
                version = Integer.parseInt(contentVersion);
            }
            catch (NumberFormatException e)
            {
                log.debug("Invalid content-version: " + contentVersion);
            }
        }
        return version;
    }
}
