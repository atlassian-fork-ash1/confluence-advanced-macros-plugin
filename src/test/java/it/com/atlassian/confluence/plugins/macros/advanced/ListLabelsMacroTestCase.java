package it.com.atlassian.confluence.plugins.macros.advanced;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class ListLabelsMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    private void assertBucket(String bucket, List<String> links)
    {
        String bucketUpperCased = StringUtils.upperCase(bucket);
        assertElementPresentByXPath("//div[@class='wiki-content']//div[contains(@class, 'labelsAlphaList')]/ol/li/div[text()='" + bucketUpperCased + "']");

        int linkCount = links.size();
        for (int i = 1; i <= linkCount; ++i)
        {
            assertEquals(
                    links.get(i - 1),
                    getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class, 'labelsAlphaList')]/ol/li[div[text()='" + bucketUpperCased + "']]/ul[@class='labelList']/li[" + i + "]/a")
            );
        }
    }

    public void testListLabelsInUnmergedBucketsInTheCurrentSpace()
    {
        for (char i = 'a', j = 'z'; i <= j; ++i)
        {
            for (int x = 0; x < 10; ++x)
            {
                createPage(TEST_SPACE_KEY, String.valueOf(i) + "-" + x, StringUtils.EMPTY, Arrays.asList(String.valueOf(i) + "-" + x));
            }
        }

        for (int i = 0; i < 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(String.valueOf(i)));

        long testPageId = createPage(TEST_SPACE_KEY, "testListLabelsInUnmergedBucketsInTheCurrentSpace", "{listlabels}");

        viewPageById(testPageId);

            List<String> links = new ArrayList<String>();
        for (char i = 'a', j = 'z'; i <= j; ++i)
        {
            links.clear();

            for (int x = 0; x < 10; ++x)
            {
                links.add(String.valueOf(i) + "-" + x);
            }

            assertBucket(String.valueOf(i), links);
        }

        links.clear();
        links.addAll(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
        assertBucket("0-9", links);
    }

    public void testListLabelsInUnmergedBucketsInOtherSpace()
    {
        for (char i = 'a', j = 'z'; i <= j; ++i)
        {
            for (int x = 0; x < 10; ++x)
            {
                createPage(TEST_SPACE_KEY, String.valueOf(i) + "-" + x, StringUtils.EMPTY, Arrays.asList(String.valueOf(i) + "-" + x));
            }
        }

        for (int i = 0; i < 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(String.valueOf(i)));

        long testPageId = createPage(DEMO_SPACE_KEY, "testListLabelsInUnmergedBucketsInOtherSpace", "{listlabels:spaceKey=" + TEST_SPACE_KEY + "}");

        viewPageById(testPageId);

            List<String> links = new ArrayList<String>();
        for (char i = 'a', j = 'z'; i <= j; ++i)
        {
            links.clear();

            for (int x = 0; x < 10; ++x)
            {
                links.add(String.valueOf(i) + "-" + x);
            }

            assertBucket(String.valueOf(i), links);
        }

        links.clear();
        links.addAll(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
        assertBucket("0-9", links);
    }
}
