package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Macro to display a list of labels related to the page the macro is used on.
 * <p>
 * This macro has the optional 'labels' parameter, which allows the user to define
 * a comma-separated list of labels whose related labels should be displayed.
 */
public class RelatedLabelsMacro extends BaseMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/relatedlabelsmacro.vm";

    private static final String LABELS = "labels";

    private LabelManager labelManager;

    private ConfluenceActionSupport confluenceActionSuppport;

    public void setLabelManager(LabelManager manager)
    {
        this.labelManager = manager;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        if (!(renderContext instanceof PageContext))
            return RenderUtils.blockError(
                    getConfluenceActionSuppport().getText("relatedlabels.error.unable-to-render"),
                    getConfluenceActionSuppport().getText("relatedlabels.error.can-only-be-used-in-pages-or-blogposts")
            );

        PageContext pageContext = (PageContext) renderContext;

        Map<String, Object> contextMap = getMacroVelocityContext();

        List<Label> contents = new LinkedList<Label>();
        List<Label> allLabels = new ArrayList<Label>();

        String labels = (String) parameters.get(LABELS);

        // If there are no labels defined...
        if (!TextUtils.stringSet(labels))
        {
            // Grab the related labels for the labels on the current page
            allLabels.addAll(pageContext.getEntity().getLabels());
        }
        else
        {
            // labels is a comma separated list of label definitions.
            StringTokenizer tokenizer = new StringTokenizer(labels, ", ");

            while (tokenizer.hasMoreTokens())
            {
                String labelName = tokenizer.nextToken().trim();
                ParsedLabelName ref = LabelParser.parse(labelName);

                if (ref != null)
                {
                    Label label = labelManager.getLabel(ref);

                    if (label != null)
                        allLabels.add(label);
                }
            }
        }

        // For all the labels, grab the related labels and add them to the list
        for (Label label : allLabels)
        {
            List<Label> relatedLabels = labelManager.getRelatedLabels(label);

            // Iterate through the related labels for a particular label
            for (Label currentRelLabel : relatedLabels)
            {

                // Add them to the list if it's unique
                if (!contents.contains(currentRelLabel))
                    contents.add(currentRelLabel);
            }
        }

        contextMap.put("relatedLabels", contents);

        return renderRelatedLabels(contextMap);
    }
    
    public ConfluenceActionSupport getConfluenceActionSuppport()
    {
        if (null == confluenceActionSuppport)
            confluenceActionSuppport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSuppport;
    }
    
    ///CLOVER:OFF
    protected Map<String, Object> getMacroVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }

    protected String renderRelatedLabels(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);
    }
    ///CLOVER:ON
}
