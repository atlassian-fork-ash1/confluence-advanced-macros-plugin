package com.atlassian.confluence.plugins.macros.advanced.xhtml.deprecated;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.render.xhtml.DefaultHtmlToXmlConverter;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.content.render.xhtml.ResettableXmlEventReader;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;

/**
 * Responsible for stripping paragraph tags.
 *
 * Copied over from core (mainly because we're on the eve of releasing 4.0 and we don't currently have the luxury to
 * release a version of Confluence in which to compile this plugin against).
 */
public class HTMLParagraphStripper
{
    private final XmlEventReaderFactory xmlEventReaderFactory;
    private final XMLOutputFactory xmlOutputFactory;

    public HTMLParagraphStripper(XMLOutputFactory xmlOutputFactory, XmlEventReaderFactory xmlEventReaderFactory)
    {
        this.xmlOutputFactory = xmlOutputFactory;
        this.xmlEventReaderFactory = xmlEventReaderFactory;
    }

    /**
     * Removes the first encountered paragraph from a fragment of xhtml if it is the first tag.
     * @param xml xml
     * @return stripped xml
     * @throws XMLStreamException ex
     */
    public String stripFirstParagraph(String xml) throws XMLStreamException
    {
        StringWriter outputWriter = new StringWriter();

        XMLEventWriter eventWriter = xmlOutputFactory.createXMLEventWriter(outputWriter);
        XMLEventReader eventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(xml));

        if (!eventReader.hasNext())
            return xml;

        if (!eventReader.peek().isStartElement() || !"p".equals(eventReader.peek().asStartElement().getName().getLocalPart()))
            return xml;

        XMLEventReader bodyEventReader = xmlEventReaderFactory.createXmlFragmentBodyEventReader(eventReader);
        try
        {
            eventWriter.add(bodyEventReader);
        }
        finally
        {
            bodyEventReader.close();
        }

        eventWriter.add(eventReader);
        eventWriter.flush();

        return outputWriter.toString();
    }

    /**
     * Removes the first encountered paragraph from a fragment of xhtml if it is the first tag.
     * @param xml xml
     * @return stripped xml
     * @throws XMLStreamException ex
     */
    public String stripIfSoloParagraph(String xml) throws XMLStreamException
    {
        XMLEventReader eventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(xml));

        int paragraphCount = 0;
        while (eventReader.hasNext())
        {
            XMLEvent event = eventReader.peek();

            if (event.isStartElement() && "p".equals(event.asStartElement().getName().getLocalPart()))
            {
                XMLEventReader paragraphReader = xmlEventReaderFactory.createXmlFragmentEventReader(eventReader);
                while (paragraphReader.hasNext())
                    paragraphReader.nextEvent(); // consume the paragraph fragment (so as to not count any nested paragraphs)

                if (++paragraphCount > 1)
                    break;
            }
            else
            {
                eventReader.nextEvent();
            }
        }

        if (paragraphCount == 1)
            return stripFirstParagraph(xml);
        else
            return xml;
    }
}