package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.renderer.util.RendererUtil;
import org.apache.commons.lang.StringUtils;

public class CommentUpdateItem extends AbstractUpdateItem
{
    public CommentUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, String iconClass)
    {
        super(searchResult, dateFormatter, i18n, iconClass);
    }

    @Override
    public String getUpdateTargetTitle()
    {
        if (searchResult.getDisplayTitle() == null)
            return "";

        return GeneralUtil.htmlEncode(searchResult.getDisplayTitle().substring("Re: ".length()));
    }

    @Override
    public String getBody()
    {
        String comment = searchResult.getContent();

        if (StringUtils.isNotBlank(comment))
            return GeneralUtil.shortenString(RendererUtil.summarise(comment), 120);
        else
            return "";
    }

    ///CLOVER:OFF
    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.comment";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.comment";
    }
    ///CLOVER:ON

}
