package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractGrouping implements Grouping
{
    protected final Updater updater;
    protected final List<UpdateItem> updateItems;

    public AbstractGrouping(Updater updater)
    {
        this.updater = updater;
        this.updateItems = new ArrayList<UpdateItem>();
    }

    public Updater getUpdater()
    {
        return updater;
    }

    /**
     * Adds an update item to this grouping.
     *
     * @param updateItem the update item
     * @throws IllegalArgumentException if this update item cannot be added. Check {@link #canAdd(com.atlassian.confluence.plugins.macros.advanced.recentupdate.UpdateItem)} first.
     */
    public void addUpdateItem(UpdateItem updateItem)
    {
        if (!canAdd(updateItem))
            throw new IllegalArgumentException("Cannot add: " + updateItem + " to this grouping.");

        updateItems.add(updateItem);
    }

    public List<UpdateItem> getUpdateItems()
    {
        return Collections.unmodifiableList(updateItems);
    }

    public int size()
    {
        return updateItems.size();
    }
}
