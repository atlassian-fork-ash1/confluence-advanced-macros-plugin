package it.com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.util.GeneralUtil;

import org.apache.commons.lang.StringUtils;

import it.com.atlassian.confluence.plugins.macros.advanced.AbstractConfluencePluginWebTestCaseBase;
import it.com.atlassian.confluence.plugins.macros.advanced.XmlRpcHelper;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public final class LabelledContentMacroFuncTest extends AbstractConfluencePluginWebTestCaseBase
{
    private static final String MACRO_NAME = "contentbylabel";
    private static final String TEST_LABEL_ONE = "labelone";
    private static final String TEST_LABEL_TWO = "labeltwo";
    private static final String TEST_LABEL_THREE = "labelthree";
    private static final String PERSONAL_LABEL_ONE = "my:label";
    private static final String TEST_PAGE_ONE = "Test page one";
    private static final String TEST_PAGE_TWO = "Test page two";
    private static final String TEST_PAGE_THREE = "Test page three";
    private static final String MACRO_HOST_PAGE = "Macro host page";
    private static final String TEST_SPACE_KEY = "tst";
    private static final String TEST2_SPACE_KEY = "tst2";

    private static final String TEST_GEN_USERNAME1="tstuser";
    private static final String TEST_GEN_PASSWORD1="tstpass1";
    private static final String TEST_GEN_USERNAME2="tstuser2";
    private static final String TEST_GEN_PASSWORD2="tstpass2";

    private static final String TEST_SPACE_NAME = "Test Space";

    public void testLabelledContentMacro()
    {
        createAndLabelPageOne();
        createAndLabelPageTwo();

        PageHelper macroHost = getPageHelper();
        macroHost.setSpaceKey(TEST_SPACE_KEY);
        macroHost.setTitle(MACRO_HOST_PAGE);
        macroHost.setContent("{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "," + TEST_LABEL_TWO + "}");
        assertTrue(macroHost.create());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
        assertLinkPresentWithText(TEST_PAGE_TWO);
        assertLinkPresentWithText(TEST_LABEL_TWO);
    }


    // CONF-4750
    public void testLabelledContentMacroWithSpaceInLabelsList()
    {
        createAndLabelPageOne();
        createAndLabelPageTwo();

        PageHelper macroHost = getPageHelper();
        macroHost.setSpaceKey(TEST_SPACE_KEY);
        macroHost.setTitle(MACRO_HOST_PAGE);
        macroHost.setContent("{" + MACRO_NAME + ":" + TEST_LABEL_ONE + ", " + TEST_LABEL_TWO + "}");
        assertTrue(macroHost.create());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
        assertLinkPresentWithText(TEST_PAGE_TWO);
        assertLinkPresentWithText(TEST_LABEL_TWO);
    }

    public void testLabelledContentMacroWithTitle()
    {
        final String title = "foobar";

        createAndLabelPageOne();

        PageHelper macroHost = getPageHelper();
        macroHost.setSpaceKey(TEST_SPACE_KEY);
        macroHost.setTitle(MACRO_HOST_PAGE);
        macroHost.setContent("{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|title=" + title + "}");
        assertTrue(macroHost.create());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertTextPresent(title);
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
    }

    public void testLabelledContentMacroWithExcerpt()
    {
        createPage(TEST_SPACE_KEY, "foo", "{excerpt}lions and zebras{excerpt}", Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, "bar", "{excerpt}giraffes and hyenas{excerpt}", Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, "baz", "buffalos and deer", Arrays.asList(TEST_LABEL_ONE));

        createPage(TEST_SPACE_KEY, "safari", "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|excerpt=true|sort=creation}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        logout();
        loginAsAdmin();
        // generate the excerpts
        gotoPage("/display/" + TEST_SPACE_KEY + "/foo");
        gotoPage("/display/" + TEST_SPACE_KEY + "/bar");
        gotoPage("/display/" + TEST_SPACE_KEY + "/baz");
        gotoPage("/display/" + TEST_SPACE_KEY + "/safari");

        assertTextPresent("safari");
        assertTextPresent("lions and zebras");
        assertTextPresent("giraffes and hyenas");
        assertTextPresent("baz");
        assertTextNotPresent("buffalos and deer");
    }

    public void testLabelledContentMacroWithContentType()
    {
        String blogTitle = "bloggie blog";

        createAndLabelPageOne();

        // Create a blog post and label it
        BlogPostHelper blogPostHelper = getBlogPostHelper();
        blogPostHelper.setSpaceKey(TEST_SPACE_KEY);
        blogPostHelper.setTitle(blogTitle);
        blogPostHelper.setLabels(Arrays.asList(TEST_LABEL_ONE));
        blogPostHelper.setCreationDate(new Date());
        assertTrue(blogPostHelper.create());

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|type=" + ContentTypeEnum.BLOG + "}",
            Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        // The page should not be listed
        assertLinkNotPresentWithText(TEST_PAGE_ONE);
        // But the blog post should
        assertLinkPresentWithText(blogTitle);
        assertLinkPresentWithText(TEST_LABEL_ONE);
    }

    public void testLabelledContentMacroWithIllegalContentType()
    {
        createAndLabelPageOne();

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|type=xxx}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        // The error text will be in the title attribute of the "Broken macro" placeholder image.
        assertThat(getPageSource(), containsString(GeneralUtil.htmlEncode("'xxx' is not a valid content type")));
        assertLinkNotPresentWithText(TEST_PAGE_ONE);
        assertLinkNotPresentWithText(TEST_LABEL_ONE);
    }

    public void testLabelledContentMacroWithShowLabelsFalse()
    {
        createAndLabelPageOne();

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|showLabels=false}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkNotPresentWithText(TEST_LABEL_ONE);
    }

    public void testLabelledContentMacroInSpace()
    {
        final String pageTitle = "another page";

        createAndLabelPageOne();

        final String testSpace2Key = "tst2";
        createSpace(testSpace2Key, testSpace2Key, StringUtils.EMPTY);
        createPage(testSpace2Key, pageTitle, "", Arrays.asList(TEST_LABEL_ONE));

        // Test with 'space' param
        long id = createPage(testSpace2Key, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|space=" + TEST_SPACE_KEY + "}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + testSpace2Key + "/" + MACRO_HOST_PAGE);
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkNotPresentWithText(pageTitle);

        // Test with 'key' param to select a space
        PageHelper pageHelper = getPageHelper(id);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|key=" + TEST_SPACE_KEY + "}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/display/" + testSpace2Key + "/" + MACRO_HOST_PAGE);
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkNotPresentWithText(pageTitle);

    }

    public void testLabelledContentMacroInMultipleSpaces()
    {
        final String otherSpaceKey = "OSK";

        createSpace(otherSpaceKey, otherSpaceKey, StringUtils.EMPTY);
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(otherSpaceKey, TEST_PAGE_TWO, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        // Test with 'space' param
        long id = createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|key=" + TEST_SPACE_KEY + "," + otherSpaceKey + "|sort=title}");

        getIndexHelper().update();
        viewPageById(id);

        assertEquals(
                TEST_PAGE_ONE,
                getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]")
        );

        assertEquals(
                TEST_PAGE_TWO,
                getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]")
        );
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }


    public void testLabelledContentMacroWithPersonalLabels()
    {
        // Create a user that can view the page
        createUser(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1, "test user 1");

        XmlRpcHelper xmlrpc = XmlRpcHelper.getInstance(getConfluenceWebTester());
        xmlrpc.grantViewSpacePermission(TEST_GEN_USERNAME1, TEST_SPACE_KEY);
        xmlrpc.grantSpacePermission(XmlRpcHelper.CREATEEDIT_PAGE_PERMISSION, TEST_GEN_USERNAME1, TEST_SPACE_KEY);

        // have the admin create the pages
        long pageId = createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "");
        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "}", Collections.<String>emptyList());

        // have the test user add the label
        logout();
        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);
        addLabelToPage(TEST_LABEL_ONE, pageId);
        addLabelToPage(PERSONAL_LABEL_ONE, pageId);

        logout();
        loginAsAdmin();
        getIndexHelper().update();

        logout();
        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        // Page 1
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
        // Personal label should be visible
        assertLinkPresentWithText(PERSONAL_LABEL_ONE);
    }

    public void testLabelledContentMacroWithPersonalLabelsAsDifferentUser()
    {
        createUser(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1, "test user 1");

        XmlRpcHelper xmlrpc = XmlRpcHelper.getInstance(getConfluenceWebTester());
        xmlrpc.grantViewSpacePermission(TEST_GEN_USERNAME1, TEST_SPACE_KEY);
        xmlrpc.grantSpacePermission(XmlRpcHelper.CREATEEDIT_PAGE_PERMISSION, TEST_GEN_USERNAME1, TEST_SPACE_KEY);

        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE, PERSONAL_LABEL_ONE));

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "}", Collections.<String>emptyList());

        getIndexHelper().update();

        // Login as the test user and view the labels (including personal one)
        logout();
        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        // Page 1
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
        // Personal label should not be visible
        assertLinkNotPresentWithText(PERSONAL_LABEL_ONE);
    }

    public void testLabelledContentMacroWithAndOperator()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_TWO, "", Arrays.asList(TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_THREE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_THREE));

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "," + TEST_LABEL_TWO + "|operator=AND}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        // Page 1
        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_LABEL_ONE);
        // Page 2
        assertLinkNotPresentWithText(TEST_PAGE_TWO);
        assertLinkPresentWithText(TEST_LABEL_TWO);

        assertLinkNotPresentWithText(TEST_PAGE_THREE);
    }

    // CONF-8703 -- missing label was being treated as not being part of search!
    public void testLabelledContentMacroWithAndOperatorAndNonexistentLabel()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_TWO, "", Arrays.asList(TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_THREE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_THREE));

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + ",monkeybutter|operator=AND}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertLinkNotPresentWithText(TEST_PAGE_ONE);
        assertLinkNotPresentWithText(TEST_PAGE_TWO);
        assertLinkNotPresentWithText(TEST_PAGE_THREE);
    }

    public void testLabelledContentMacroWithOrOperatorAndNonexistentLabel()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_TWO, "", Arrays.asList(TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, TEST_PAGE_THREE, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_THREE));

        createPage(TEST_SPACE_KEY, "Page Four", "", Arrays.asList("monkeybutter"));

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "," + TEST_LABEL_TWO + "|operator=OR}",
                Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertLinkPresentWithText(TEST_PAGE_ONE);
        assertLinkPresentWithText(TEST_PAGE_TWO);
        assertLinkPresentWithText(TEST_PAGE_THREE);
        assertLinkNotPresentWithText("Page Four");
    }

    // CONFDEV-5767
    public void testContentExcerptIsRenderedHtml()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "{excerpt}*Example page content*{excerpt}", Arrays.asList(TEST_LABEL_ONE));
        String macro = "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|excerpt=true}";

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, macro, Collections.emptyList());
        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);
        assertTextPresent("Example page content");
        assertTextNotPresent("<strong>");
    }

    // CONF-12749
    public void testLabelledContentMacroWithSpaceSpecificLinks()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE));

        String macro1 = "{" + MACRO_NAME + ":" + TEST_LABEL_ONE + "|space=" + TEST_SPACE_KEY + "}";

        createPage(TEST_SPACE_KEY, MACRO_HOST_PAGE, macro1, Collections.<String>emptyList());

        getIndexHelper().update();

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        assertEquals("/confluence/label/" + TEST_SPACE_KEY + "/" + TEST_LABEL_ONE, getElementAttributeByXPath("//*[contains(@class, 'content-by-label')]/li[1]//*[contains(@class, 'aui-label') or contains(@class, 'label')]/a", "href"));
    }

    private void createAndLabelPageTwo()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_TWO, "", Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));
    }

    private void createAndLabelPageOne()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, "", Arrays.asList(TEST_LABEL_ONE));
    }

    public void testMaxParameterLimitsNumberOfItemsShown()
    {
        final int maxItems = 1;

        for (int i = 0; i < 2; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testMaxParameterLimitsNumberOfItemsShown",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|max=" + maxItems + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);
        assertEquals("Page 0", getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li//a"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testMaxResultsParameterLimitsNumberOfItemsShown()
    {
        final int maxItems = 1;

        for (int i = 0; i < 2; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testMaxResultsParameterLimitsNumberOfItemsShown",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|maxResults=" + maxItems + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);
        assertEquals("Page 0", getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li//a"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-13666">CONF-13666</a>
     */
    public void testDefaultMaxNotIgnored()
    {
        final int maxItems = 15;

        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testDefaultMaxNotIgnored",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|max=" + maxItems + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);

        for (int i = 0; i < 15; ++i)
            assertEquals("Page " + i, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[" + (i + 1) + "]//a[1]"));

        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[16]");
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-13666">CONF-13666</a>
     */
    public void testDefaultMaxResultsNotIgnored()
    {
        final int maxItems = 15;

        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testDefaultMaxResultsNotIgnored",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|maxResults=" + maxItems + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);

        for (int i = 0; i < 15; ++i)
            assertEquals("Page " + i, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[" + (i + 1) + "]//a[1]"));

        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[16]");
    }

    public void testContentFilterByAuthor()
    {
        createUser(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1, TEST_GEN_USERNAME1, new String[] { "confluence-administrators" });
        createUser(TEST_GEN_USERNAME2, TEST_GEN_PASSWORD2, TEST_GEN_USERNAME2, new String[] { "confluence-administrators" });
        logout();

        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);
        final String title1 = "Test page by " + TEST_GEN_USERNAME1;
        createPage(TEST_SPACE_KEY, title1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        logout();

        login(TEST_GEN_USERNAME2, TEST_GEN_PASSWORD2);
        final String title2 = "Test page by " + TEST_GEN_USERNAME2;
        createPage(TEST_SPACE_KEY, title2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        logout();

        loginAsAdmin();
        final String title3 = "Test page by admin";
        createPage(TEST_SPACE_KEY, title3, StringUtils.EMPTY, Arrays.asList());

        long testPageId = createPage(TEST_SPACE_KEY, "testContentFilterByExplicitContributors", "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation}");
        getIndexHelper().update();
        viewPageById(testPageId);
        assertLinkPresentWithExactText(title1);
        assertLinkPresentWithExactText(title2);
        assertLinkNotPresentWithExactText(title3);

        long testPage2Id = createPage(TEST_SPACE_KEY, "testContentFilterByExplicitContributors2", "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|author=" + TEST_GEN_USERNAME1 + "}");
        getIndexHelper().update();
        viewPageById(testPage2Id);
        assertLinkPresentWithExactText(title1);
        assertLinkNotPresentWithExactText(title2);
        assertLinkNotPresentWithExactText(title3);

        // Test multiple authors
        long testPage3Id = createPage(TEST_SPACE_KEY, "testContentFilterByExplicitContributors3", "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|author=" + TEST_GEN_USERNAME1 + "," + TEST_GEN_USERNAME2 + "}");
        getIndexHelper().update();
        viewPageById(testPage3Id);
        assertLinkPresentWithExactText(title1);
        assertLinkPresentWithExactText(title2);
        assertLinkNotPresentWithExactText(title3);
    }

    public void testContentFilterByLabelExclusion()
    {
        final String titleOfPageWithoutExcludedLabel = "Page without excluded label";

        createPage(TEST_SPACE_KEY, "Page with excluded label", StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));
        createPage(TEST_SPACE_KEY, titleOfPageWithoutExcludedLabel, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testContentFilterByLabelExclusion",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + ",-" + TEST_LABEL_TWO + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPageWithoutExcludedLabel, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testContentFilterByExplicityLabels()
    {
        final String pageWithAllLabels = "Page with all labels";

        createPage(TEST_SPACE_KEY, "Page without all labels", StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, pageWithAllLabels, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE, TEST_LABEL_TWO));

        long testPageId = createPage(TEST_SPACE_KEY, "testContentFilterByExplicityLabels",
                "{" + MACRO_NAME + ":labels=+" + TEST_LABEL_ONE + ",+" + TEST_LABEL_TWO + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(pageWithAllLabels, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testSortPageAndBlogPostsByTitle()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";
        final String blogTitle = "page 0";

        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        addLabelToBlogPost(TEST_LABEL_ONE, createBlogPost(TEST_SPACE_KEY, blogTitle, StringUtils.EMPTY));

        long testPageId = createPage(TEST_SPACE_KEY, "testSortPageAndBlogPostsByTitle",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=title}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(blogTitle, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[3]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[4]");
    }

    public void testSortByTitle()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";

        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByTitle",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=title}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testSortByTitleReversed()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";

        createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByTitleReversed",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=title|reverse=true}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testSortByCreationDate()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";
        final String titleOfPage3 = "page 3";

        createPage(TEST_SPACE_KEY, titleOfPage3, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByCreationDate",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage3, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[3]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[4]");
    }

    public void testSortByCreationDateReversed()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";
        final String titleOfPage3 = "page 3";

        createPage(TEST_SPACE_KEY, titleOfPage3, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByCreationDateReversed",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|reverse=true}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertEquals(titleOfPage3, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[3]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[4]");
    }

    public void testSortByModificationDate()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";

        final long idOfPage1 = createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        updatePage(idOfPage1);

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByModificationDate",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=modified}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testSortByModificationDateReversed()
    {
        final String titleOfPage1 = "page 1";
        final String titleOfPage2 = "page 2";

        final long idOfPage1 = createPage(TEST_SPACE_KEY, titleOfPage1, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(TEST_SPACE_KEY, titleOfPage2, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        updatePage(idOfPage1);

        long testPageId = createPage(TEST_SPACE_KEY, "testSortByModificationDateReversed",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=modified|reverse=true}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(titleOfPage1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(titleOfPage2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testFilterContentByPage()
    {
        final String pageTitle = "page 1";

        createPage(TEST_SPACE_KEY,  pageTitle, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        addLabelToBlogPost(TEST_LABEL_ONE, createBlogPost(TEST_SPACE_KEY, "blog post 1", StringUtils.EMPTY));

        long testPageId = createPage(TEST_SPACE_KEY, "testFilterContentByPage",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|type=page}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(pageTitle, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }


    public void testFilterContentByBlogPost()
    {
        final String blogTitle = "blog 1";

        createPage(TEST_SPACE_KEY,  "page 1", StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        addLabelToBlogPost(TEST_LABEL_ONE, createBlogPost(TEST_SPACE_KEY, blogTitle, StringUtils.EMPTY));

        long testPageId = createPage(TEST_SPACE_KEY, "testFilterContentByBlogPost",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|type=blogpost}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(blogTitle, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testShowContentByLabelFromAllSpace()
    {
        final String pageTitleInTstSpace = "page 0 in tst";
        final String pageTitleInTst2Space = "page 0 in tst2";
        final String otherSpaceKey = "tst2";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        createPage(TEST_SPACE_KEY, pageTitleInTstSpace, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(otherSpaceKey, pageTitleInTst2Space, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testShowContentByLabelFromAllSpace",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@all}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(pageTitleInTstSpace, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(pageTitleInTst2Space, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testShowContentByLabelFromCurrentSpaceOnly()
    {
        final String pageTitleInTstSpace = "page 0 in tst";
        final String pageTitleInTst2Space = "page 0 in tst2";
        final String otherSpaceKey = "tst2";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        createPage(TEST_SPACE_KEY, pageTitleInTstSpace, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(otherSpaceKey, pageTitleInTst2Space, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testShowContentByLabelFromCurrentSpaceOnly",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@self}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(pageTitleInTstSpace, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    private void createPersonalSpace()
    {
        gotoPage("/spaces/createpersonalspace.action");
        setWorkingForm("create-personal-space-form");
        submit("create");
    }

    public void testShowContentByLabelFromPersonalSpaceOnly()
    {
        final String personalSpacePage = "personal 0";

        createPersonalSpace();
        createPage(TEST_SPACE_KEY, "page 0", StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage("~" + getConfluenceWebTester().getCurrentUserName(), personalSpacePage, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testShowContentByLabelFromPersonalSpaceOnly",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@personal}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(personalSpacePage, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    private void markSpaceAsFavourite(final String spaceKey)
    {
        gotoPage("/spacedirectory/view.action");
        gotoPage("/json/addspacetofavourites.action?key=" + spaceKey + "&atl_token=" + getElementAttributeByXPath("//meta[@id='atlassian-token']", "content"));
    }

    public void testShowContentByLabelFromFavoriteSpaceOnly()
    {
        final String pageTitleInTstSpace = "page 0 in tst";
        final String pageTitleInTst2Space = "page 0 in tst2";
        final String otherSpaceKey = "tst2";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        markSpaceAsFavourite(otherSpaceKey);

        createPage(TEST_SPACE_KEY, pageTitleInTstSpace, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(otherSpaceKey, pageTitleInTst2Space, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testShowContentByLabelFromFavoriteSpaceOnly",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@favorite}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(pageTitleInTst2Space, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    // ADVMACROS-129
    public void testShowContentByLabelWithMultipleAuthorsAllowed()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_TITLE, "{contentbylabel:xxx|author=akjhsdkja,gmfd}");

        viewPage(TEST_SPACE_KEY, TEST_PAGE_TITLE);

        // macro should be allowed and found nothing
        assertTextPresent("no content with the specified label");
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-12749">CONF-12749</a>
     */
    public void testLabelLinkPointsToSpaceLabelWhenMacroIsShowingLabelledContentOfSelfSpace()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testLabelLinkPointsToSpaceLabelWhenMacroIsShowingLabelledContentOfSelfSpace",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@self}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(TEST_PAGE_ONE, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");

        clickLinkWithText(TEST_LABEL_ONE);
        assertTitleEquals("Labelled content - " + TEST_SPACE_NAME + " - Confluence");

        String labelname = getElementTextByXPath("//div[@class='labels-main-description']/ul[contains(@class, 'label-list')]/li[contains(@class, 'aui-label')]/a");
        assertEquals(TEST_LABEL_ONE, labelname);
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-12749">CONF-12749</a>
     */
    public void testLabelLinkPointsToSpaceLabelWhenMacroIsShowingLabelledContentOfOneSpecificSpace()
    {
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testLabelLinkPointsToSpaceLabelWhenMacroIsShowingLabelledContentOfOneSpecificSpace",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=" + TEST_SPACE_KEY + "}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(TEST_PAGE_ONE, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[2]");

        clickLinkWithText(TEST_LABEL_ONE);
        assertTitleEquals("Labelled content - " + TEST_SPACE_NAME + " - Confluence");

        String labelname = getElementTextByXPath("//div[@class='labels-main-description']/ul[contains(@class, 'label-list')]/li[contains(@class, 'aui-label')]/a");
        assertEquals(TEST_LABEL_ONE, labelname);
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-12749">CONF-12749</a>
     */
    public void testLabelLinkPointsToGlobalLabelWhenMacroIsShowingLabelledContentFromMultiSpaces()
    {
        final String otherSpaceKey = "tst2";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        createPage(TEST_SPACE_KEY, TEST_PAGE_ONE, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));
        createPage(otherSpaceKey, TEST_PAGE_ONE, StringUtils.EMPTY, Arrays.asList(TEST_LABEL_ONE));

        long testPageId = createPage(TEST_SPACE_KEY, "testLabelLinkPointsToGlobalLabelWhenMacroIsShowingLabelledContentFromMultiSpaces",
                "{" + MACRO_NAME + ":labels=" + TEST_LABEL_ONE + "|sort=creation|spaces=@global}");
        getIndexHelper().update();

        viewPageById(testPageId);

        assertEquals(TEST_PAGE_ONE, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(TEST_PAGE_ONE, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresentByXPath("//*[contains(@class, 'content-by-label')]/li[3]");

        clickLinkWithText(TEST_LABEL_ONE);
        assertTitleEquals("Labelled content - Confluence");

        String labelname = getElementTextByXPath("//div[@class='labels-main-description']/ul[contains(@class, 'label-list')]/li[contains(@class, 'aui-label')]/a");
        assertEquals(TEST_LABEL_ONE, labelname);
    }

//    public void testInvalidSpaceKeysReported()
//    {
//        final String invalidSpaceKey = "foobar";
//        final PageHelper pageHelper = getPageHelper();
//
//        pageHelper.setSpaceKey(TEST_SPACE_KEY);
//        pageHelper.setTitle("testInvalidSpaceKeysReported");
//        pageHelper.setContent("{" + MACRO_NAME + ":spaces=" + invalidSpaceKey +"}");
//
//        assertTrue(pageHelper.create());
//
//        getIndexHelper().update();
//
//        viewPageById(pageHelper.getId());
//        assertEquals(
//                MACRO_NAME + ": '" + invalidSpaceKey +"' is not an existing space's key",
//                getElementTextByXPath("//div[@class='wiki-content']/div[@class='error']/span[@class='error']")
//        );
//    }

    public void testContentFilterByPersonalLabel()
    {
        final String pageWithPersonalLabel = "pageWithPersonalLabel";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String personalLabel = "my:" + TEST_LABEL_ONE;
        final String globalLabel= TEST_LABEL_ONE;

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel, StringUtils.EMPTY, Arrays.asList(personalLabel));
        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByPersonalLabel",
                "{" + MACRO_NAME + ":labels=" + personalLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(pageWithPersonalLabel, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testContentFilterByGlobalLabel()
    {
        final String pageWithPersonalLabel = "pageWithPersonalLabel";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String personalLabel = "my:" + TEST_LABEL_ONE;
        final String globalLabel= TEST_LABEL_ONE;

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel, StringUtils.EMPTY, Arrays.asList(personalLabel));
        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByGlobalLabel",
                "{" + MACRO_NAME + ":labels=" + globalLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(pageWithGlobalLabel, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testContentFilterByFavouriteLabel()
    {
        final String favoritePage = "favoritePage";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String favoriteLabel = "my:favourite"; /* Only works with my:favourite */
        final String globalLabel= "favourite";

        final long favoritePageId = createPage(TEST_SPACE_KEY, favoritePage, StringUtils.EMPTY);
        final PageHelper favoritePageHelper = getPageHelper(favoritePageId);

        assertTrue(favoritePageHelper.read());
        favoritePageHelper.markFavourite();
        assertTrue(favoritePageHelper.isFavorite());

        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByFavouriteLabel",
                "{" + MACRO_NAME + ":labels=" + favoriteLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(favoritePage, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testContentFilterByMultipleOptionalPersonalLabels()
    {
        final String pageWithPersonalLabel1 = "pageWithPersonalLabel1";
        final String pageWithPersonalLabel2 = "pageWithPersonalLabel2";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel1, StringUtils.EMPTY, Arrays.asList(personalLabel1));
        createPage(TEST_SPACE_KEY, pageWithPersonalLabel2, StringUtils.EMPTY, Arrays.asList(personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleOptionalPersonalLabels",
                "{" + MACRO_NAME + ":sort=modified|reverse=true|labels=" + personalLabel1 + "," + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(pageWithPersonalLabel2, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertEquals(pageWithPersonalLabel1, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[2]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[3]");
    }

    public void testContentFilterByMultipleCompulsoryPersonalLabels()
    {
        final String pageWithAllPersonalLabels = "pageWithAllPersonalLabels";
        final String pageWithoutAllPersonalLabels = "pageWithoutAllPersonalLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithAllPersonalLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1, personalLabel2));
        createPage(TEST_SPACE_KEY, pageWithoutAllPersonalLabels, StringUtils.EMPTY, Arrays.asList(personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleCompulsoryPersonalLabels",
                "{" + MACRO_NAME + ":sort=modified|reverse=true|labels=+" + personalLabel1 + ",+" + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(pageWithAllPersonalLabels, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testContentFilterByExcludingPersonalLabels()
    {
        final String pageWithoutExcludedLabels = "pageWithoutExcludedLabels";
        final String pageWithExcludedLabels = "pageWithExcludedLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithoutExcludedLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1));
        createPage(TEST_SPACE_KEY, pageWithExcludedLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1, personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByExcludingPersonalLabels",
                "{" + MACRO_NAME + ":sort=modified|reverse=true|labels=" + personalLabel1 + ",-" + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertEquals(pageWithoutExcludedLabels, getElementTextByXPath("//*[contains(@class, 'content-by-label')]/li[1]//a[1]"));
        assertElementNotPresent("//*[contains(@class, 'content-by-label')]/li[2]");
    }

    public void testSearchesAllSpacesByDefault()
    {
        createSpace(TEST2_SPACE_KEY, "Test Space Two", "Test Space Two");

        createPage(TEST_SPACE_KEY, "Page One", "", Arrays.asList("label1"));
        createPage(TEST2_SPACE_KEY, "Page Two", "", Arrays.asList("label1"));

        final long macroContainerPageId = createPage(TEST_SPACE_KEY, "Content By Label", "{" + MACRO_NAME + ":labels=label1}");
        getIndexHelper().update();
        viewPageById(macroContainerPageId);
        assertLinkPresentWithText("Page One");
        assertLinkPresentWithText("Page Two");
    }


    // CONF-15440
    public void testXSSinTitle()
    {
        final String titleScript = "<script>alert('Vulnerable')</script>";

        PageHelper macroHost = getPageHelper();
        macroHost.setSpaceKey(TEST_SPACE_KEY);
        macroHost.setTitle(MACRO_HOST_PAGE);
        macroHost.setContent(String.format("{%s:%s|title=%s}", MACRO_NAME, TEST_LABEL_ONE, titleScript));
        assertTrue(macroHost.create());

        gotoPage("/display/" + TEST_SPACE_KEY + "/" + MACRO_HOST_PAGE);

        // the title should display as normal text i.e. already escaped
        assertTextPresent(titleScript);
     }
}
