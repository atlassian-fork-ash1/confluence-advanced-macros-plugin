package com.atlassian.confluence.plugins.macros.advanced.xhtml;


import javax.xml.stream.XMLStreamException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.view.excerpt.ExcerptConfig;
import com.atlassian.confluence.content.render.xhtml.view.excerpt.Excerpter;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to handle all the excerpting needs of the advanced macros.
 *
 * The idea is to use the core default excerpter if possible, specially for {@link ExcerptType#RENDERED}.
 *
 * Use {@link ExcerptType#LEGACY} and add methods here as needed to maintain compatibility with how excerpts where rendered before
 */
public class AdvancedMacrosExcerpter
{
    private static final Logger log = LoggerFactory.getLogger(AdvancedMacrosExcerpter.class);

    private Excerpter excerpter;
    private ExcerptHelper excerptHelper;
    private Renderer viewRenderer;
    private I18NBeanFactory i18NBeanFactory;
    private LocaleManager localeManager;

    public String createExcerpt(ContentEntityObject contentEntityObject, String outputType)
    {
        try
        {
            ExcerptConfig excerptConfig = ExcerptConfig.builder().ignoreUserDefinedExcerpt(false).maxBlocks(3).maxCharCount(300).build();
            return excerpter.createExcerpt(contentEntityObject, outputType, excerptConfig);
        }
        catch (XMLStreamException e)
        {
            log.warn("Unable to render excerpt", e);
            return RenderUtils.error(getI18nBean().getText("advanced.macros.excerpt.error"));
        }
    }

    //Used in labelledcontent.vm
    public String createExcerpt(ContentEntityObject entity, ExcerptType excerptType)
    {
        String excerpt = null;

        if (excerptType == ExcerptType.LEGACY)
        {
            excerpt = viewRenderer.render(excerptHelper.getExcerpt(entity), new DefaultConversionContext(entity.toPageContext()));
        }
        else if (excerptType == ExcerptType.RENDERED)
        {
            excerpt = createExcerpt(entity, entity.toPageContext().getOutputType());
        }

        return StringUtils.defaultIfBlank(excerpt, "");
    }

    public String createExcerpt(ContentEntityObject entity, ExcerptType excerptType, ConversionContext conversionContext, String legacyWrapperStart, String legacyWrapperEnd)
    {
        String excerpt = null;

        if (excerptType == ExcerptType.LEGACY)
        {
            String renderedExcerpt = GeneralUtil.htmlEncode(excerptHelper.getExcerptSummary(entity));

            if (StringUtils.isNotEmpty(renderedExcerpt))
            {
                excerpt = legacyWrapperStart + renderedExcerpt + legacyWrapperEnd;
            }
        }
        else if (excerptType == ExcerptType.RENDERED)
        {
            excerpt = createExcerpt(entity, conversionContext.getOutputType());
        }

        return StringUtils.defaultIfBlank(excerpt, "");
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    public void setExcerpter(Excerpter excerpter)
    {
        this.excerpter = excerpter;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        this.excerptHelper = excerptHelper;
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        this.viewRenderer = viewRenderer;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }
}
