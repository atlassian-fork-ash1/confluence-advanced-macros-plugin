package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RelatedLabelsMacroTestCase extends TestCase
{
    private ConfluenceActionSupport confluenceActionSupport;

    private LabelManager labelManager;

    private RelatedLabelsMacro relatedLabelsMacro;

    private Map<String, String> macroParams;

    private Page pageToRenderOn;

    private Map<String, Object> macroVelocityContext;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        confluenceActionSupport = mock(ConfluenceActionSupport.class);
        labelManager = mock(LabelManager.class);

        relatedLabelsMacro = new TestRelatedLabelsMacro();

        macroParams = new HashMap<String, String>();
        pageToRenderOn = new Page();
        macroVelocityContext = new HashMap<String, Object>();
    }

    public void testErrorShownWhenNotRenderedOnPage() throws MacroException
    {
        RenderContext renderContext = mock(RenderContext.class);

        when(confluenceActionSupport.getText("relatedlabels.error.unable-to-render")).thenReturn("relatedlabels.error.unable-to-render");
        when(confluenceActionSupport.getText("relatedlabels.error.can-only-be-used-in-pages-or-blogposts")).thenReturn("relatedlabels.error.can-only-be-used-in-pages-or-blogposts");

        assertEquals(
                "<div class=\"error\"><span class=\"error\">relatedlabels.error.unable-to-render</span> relatedlabels.error.can-only-be-used-in-pages-or-blogposts</div>",
                relatedLabelsMacro.execute(macroParams, null, renderContext)
        );
    }

    public void testShowRelatedLabelsOfCurrentPageByDefault() throws MacroException
    {
        final List<Label> pageLabels = Arrays.asList(
                new Label("labelone"), new Label("labeltwo")
        );
        final List<Label> labelsRelatedToPageLabel1 = Arrays.asList(
                new Label("labelone-related1"), new Label("labelone-and-labeltwo-related")
        );
        final List<Label> labelsRelatedToPageLabel2 = Arrays.asList(
                new Label("labeltwo-related1"), new Label("labelone-and-labeltwo-related")
        );

        pageToRenderOn = new Page()
        {
            @Override
            public List<Label> getLabels()
            {
                return pageLabels;
            }
        };

        when(labelManager.getLabel(LabelParser.parse("labelone"))).thenReturn(pageLabels.get(0));
        when(labelManager.getLabel(LabelParser.parse("labeltwo"))).thenReturn(pageLabels.get(1));

        when(labelManager.getRelatedLabels(pageLabels.get(0))).thenReturn(labelsRelatedToPageLabel1);
        when(labelManager.getRelatedLabels(pageLabels.get(1))).thenReturn(labelsRelatedToPageLabel2);

        final String fakeOutput = "fakeOutput";

        relatedLabelsMacro = new TestRelatedLabelsMacro()
        {
            @Override
            protected String renderRelatedLabels(Map<String, Object> contextMap)
            {
                Collection relatedLabels = (Collection) contextMap.get("relatedLabels");

                Set<Label> allRelatedLabels = new HashSet<Label>();

                allRelatedLabels.addAll(labelsRelatedToPageLabel1);
                allRelatedLabels.addAll(labelsRelatedToPageLabel2);

                assertEquals(allRelatedLabels.size(), relatedLabels.size());
                assertTrue(relatedLabels.containsAll(allRelatedLabels));

                return fakeOutput;
            }
        };

        assertEquals(fakeOutput, relatedLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testShowRelatedLabelsOfExplicitlySpecifiedLabels() throws MacroException
    {
        final List<String> explicitLabels = Arrays.asList("labelone", "labeltwo");
        final List<Label> explicitLabelObjects = Arrays.asList(
                new Label(explicitLabels.get(0)), new Label(explicitLabels.get(1))
        );
        final List<Label> labelsRelatedToPageLabel1 = Arrays.asList(
                new Label("labelone-related1"), new Label("labelone-and-labeltwo-related")
        );
        final List<Label> labelsRelatedToPageLabel2 = Arrays.asList(
                new Label("labeltwo-related1"), new Label("labelone-and-labeltwo-related")
        );

        when(labelManager.getLabel(LabelParser.parse(explicitLabels.get(0)))).thenReturn(explicitLabelObjects.get(0));
        when(labelManager.getLabel(LabelParser.parse(explicitLabels.get(1)))).thenReturn(explicitLabelObjects.get(1));

        when(labelManager.getRelatedLabels(explicitLabelObjects.get(0))).thenReturn(labelsRelatedToPageLabel1);
        when(labelManager.getRelatedLabels(explicitLabelObjects.get(1))).thenReturn(labelsRelatedToPageLabel2);

        final String fakeOutput = "fakeOutput";

        relatedLabelsMacro = new TestRelatedLabelsMacro()
        {
            @Override
            protected String renderRelatedLabels(Map<String, Object> contextMap)
            {
                Collection relatedLabels = (Collection) contextMap.get("relatedLabels");

                Set<Label> allRelatedLabels = new HashSet<Label>();

                allRelatedLabels.addAll(labelsRelatedToPageLabel1);
                allRelatedLabels.addAll(labelsRelatedToPageLabel2);

                assertEquals(allRelatedLabels.size(), relatedLabels.size());
                assertTrue(relatedLabels.containsAll(allRelatedLabels));

                return fakeOutput;
            }
        };

        macroParams.put("labels", StringUtils.join(explicitLabels, ","));

        assertEquals(fakeOutput, relatedLabelsMacro.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testDoesNotHaveBodyAndDoesNotRenderIt()
    {
        assertFalse(relatedLabelsMacro.hasBody());
        assertEquals(RenderMode.NO_RENDER, relatedLabelsMacro.getBodyRenderMode());
    }

    public void testRendersInline()
    {
        assertFalse(relatedLabelsMacro.isInline());
    }

    private class TestRelatedLabelsMacro extends RelatedLabelsMacro
    {
        private TestRelatedLabelsMacro()
        {
            setLabelManager(labelManager);
        }

        @Override
        public ConfluenceActionSupport getConfluenceActionSuppport()
        {
            return confluenceActionSupport;
        }

        @Override
        protected Map<String, Object> getMacroVelocityContext()
        {
            return macroVelocityContext;
        }
    }
}
