package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.actions.RankedNameComparator;
import com.atlassian.confluence.labels.actions.RankedRankComparator;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class PopularLabelsMacro extends BaseMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/popularlabels.vm";
    private static final int DEFAULT_COUNT = 100;

    private LabelManager labelManager;

    private VelocityHelperService velocityHelperService;

    public void setLabelManager(LabelManager manager)
    {
        this.labelManager = manager;
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();

        String spaceKey = ((String) parameters.get("spaceKey"));
        String style = ((String) parameters.get("style"));

        Set labels;
        int maxResults = 0;

        try
        {
            maxResults = Integer.parseInt((String)parameters.get("count"));
        }
        catch(NumberFormatException e)
        {
            // ignore and use default
        }

        if(maxResults == 0)
            maxResults = DEFAULT_COUNT;

        /* Sorting of labels is dependent on the style, not the space key. See http://jira.atlassian.com/browse/CONF-12058
         * and http://jira.atlassian.com/browse/CONF-9234 
         */
        Comparator labelsComparator = StringUtils.equals("heatmap", style) ? new RankedNameComparator() : new RankedRankComparator();

        if(TextUtils.stringSet(spaceKey))
        {
            labels = labelManager.getMostPopularLabelsWithRanksInSpace(spaceKey.trim(), maxResults, labelsComparator);
            contextMap.put("spaceKey", spaceKey);
        }
        else
        {
            labels = labelManager.getMostPopularLabelsWithRanks(maxResults, labelsComparator);
        }

        contextMap.put("style", style);
        contextMap.put("labels", labels);
        
        return velocityHelperService.getRenderedTemplate(TEMPLATE_NAME, contextMap);
    }
}
