package com.atlassian.confluence.plugins.macros.advanced;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.VelocityContext;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.pages.thumbnail.Thumbnails;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.attachments.RendererAttachmentManager;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

import junit.framework.TestCase;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GalleryMacroTestCase extends TestCase
{
	private String thumbnailNotSupportedErrorText = "Thumbnails is not supported";

	private PageContext pageContext;
	private VelocityContext velocityContext;

	private AttachmentManager attachmentManager;
	private RendererAttachmentManager rendererAttachmentManager;
	private ConfluenceActionSupport confluenceActionSupport;
	private ThumbnailManager thumbnailManager;
	private LinkResolver linkResolver;
	private PageLink pageLink;
	private Thumbnails mockThumbnails;

	private String outputText = "Execution complete";
	private String title = "Gallery Title";

	@Override
	protected void setUp() throws Exception
	{
		super.setUp();

		pageContext = mock(PageContext.class);

		attachmentManager = mock(AttachmentManager.class);
		rendererAttachmentManager = mock(RendererAttachmentManager.class);
		confluenceActionSupport = mock(ConfluenceActionSupport.class);
		thumbnailManager = mock(ThumbnailManager.class);
		linkResolver = mock(LinkResolver.class);
		velocityContext = new VelocityContext();
		pageLink = mock(PageLink.class);
		mockThumbnails = mock(Thumbnails.class);
	}

	public void testWhenThumbnailsIsNotSupported() throws MacroException
	{
		Map<String, String> params;
		params = new HashMap<String, String>();

		GalleryMacro galleryMacro = new GalleryMacro()
		{
			@Override
			protected boolean isThumbnailSupported()
			{
				return false;
			}

			@Override
			public ConfluenceActionSupport getConfluenceActionSupport()
			{
				return confluenceActionSupport;
			}
		};

		when(confluenceActionSupport.getText("gallery.error.thumbnails-not-supported")).thenReturn(thumbnailNotSupportedErrorText);

		assertEquals("<p><span class=\"error\">" + thumbnailNotSupportedErrorText + "</span></p>",
				galleryMacro.execute(params, "", pageContext));
	}

    // TODOXHTML CONFDEV-1224
	public void TODOXHTML_testReverseSortingWithoutIncludeOrExcludeAttachmentAndReverseSorting() throws MacroException
	{
		final List<Attachment> attachmentsList = new ArrayList<Attachment>();

		Map<String, String> params;
		params = new HashMap<String, String>();
		params.put("title", title);
		params.put("columns", "3");
		params.put("sort", "true");
		params.put("reverse", "false");

		GalleryMacro galleryMacro = new GalleryMacro()
		{
			@Override
			protected boolean isThumbnailSupported()
			{
				return true;
			}

			@Override
			protected VelocityContext newVelocityContext()
			{
				return velocityContext;
			}

			@Override
			protected Thumbnails createThumbnails(int columns,
					List<Attachment> attachments)
			{
				// Check total size of attachments after filter
				assertEquals(3, attachments.size());

				// Check whether attachments is reverse sorted
				assertEquals("Cattle.jpg", attachments.get(0).getFileName());
				assertEquals("Beatle.jpg", attachments.get(1).getFileName());
				assertEquals("Apple.jpg", attachments.get(2).getFileName());

				return mockThumbnails;
			}

			@Override
			protected String getRenderedTemplateWithoutSwallowingErrors(
					String template, VelocityContext contextMap)
					throws Exception {
				// Check whether same mocked thumbnails is returned
				assertEquals(mockThumbnails, (Thumbnails) contextMap.get("thumbnails"));

				return outputText;
			}
		};
		setMockObjects(galleryMacro);

		Page page = new Page();
		page.setTitle("Test Page");
		page.setBodyAsString("This is page content");

		when(pageContext.getEntity()).thenReturn(page);
		when(pageContext.getOutputType()).thenReturn(RenderContextOutputType.DISPLAY);
		when(linkResolver.createLink(pageContext, "page1")).thenReturn((Link)pageLink);
		when(pageLink.getDestinationContent()).thenReturn(page);

		Attachment attachmentOne, attachmentTwo, attachmentThree;
		attachmentOne = new Attachment();
		attachmentOne.setFileName("Cattle.jpg");
		attachmentsList.add(attachmentOne);

		attachmentTwo = new Attachment();
		attachmentTwo.setFileName("Apple.jpg");
		attachmentsList.add(attachmentTwo);

		attachmentThree = new Attachment();
		attachmentThree.setFileName("Beatle.jpg");
		attachmentsList.add(attachmentThree);

		when(attachmentManager.getLatestVersionsOfAttachments(page)).thenReturn(attachmentsList);

		assertEquals(outputText, galleryMacro.execute(params, "", pageContext));
	}

	private void setMockObjects(GalleryMacro galleryMacro)
	{
		galleryMacro.setAttachmentManager(attachmentManager);
		galleryMacro.setRendererAttachmentManager(rendererAttachmentManager);
		galleryMacro.setThumbnailManager(thumbnailManager);
		galleryMacro.setLinkResolver(linkResolver);
	}

    // TODOXHTML CONFDEV-1224
	public void TODOXHTML_testIncludeAndExcludeAttachmentWithNoSorting() throws MacroException
	{
		final List<Attachment> attachmentsList = new ArrayList<Attachment>();

		Map<String, String> params;
		params = new HashMap<String, String>();
		params.put("title", title);
		params.put("columns", "3");
		params.put("include", "Apple.jpg,Beatle.jpg,Cattle.jpg");
		params.put("exclude", "Ants.xml");

		GalleryMacro galleryMacro = new GalleryMacro()
		{
			@Override
			protected boolean isThumbnailSupported()
			{
				return true;
			}

			@Override
			protected VelocityContext newVelocityContext()
			{
				return velocityContext;
			}

			@Override
			protected Thumbnails createThumbnails(int columns,
					List<Attachment> attachments)
			{
				// Check total size of attachments after filter
				assertEquals(3, attachments.size());

				// Check whether attachments is not sorted
				assertEquals("Cattle.jpg", attachments.get(0).getFileName());
				assertEquals("Apple.jpg", attachments.get(1).getFileName());
				assertEquals("Beatle.jpg", attachments.get(2).getFileName());

				return mockThumbnails;
			}

			@Override
			protected String getRenderedTemplateWithoutSwallowingErrors(
					String template, VelocityContext contextMap)
					throws Exception
					{
				// Check whether same mocked thumbnails is returned
				assertEquals(mockThumbnails, (Thumbnails) contextMap.get("thumbnails"));

				return outputText;
			}
		};
		setMockObjects(galleryMacro);

		Page page = new Page();
		page.setTitle("Test Page");
		page.setBodyAsString("This is page content");

		when(pageContext.getEntity()).thenReturn(page);
		when(pageContext.getOutputType()).thenReturn(RenderContextOutputType.DISPLAY);
		when(linkResolver.createLink(pageContext, "page1")).thenReturn((Link)pageLink);
		when(pageLink.getDestinationContent()).thenReturn(page);

		Attachment attachmentOne, attachmentTwo, attachmentThree, attachmentFour;
		attachmentOne = new Attachment();
		attachmentOne.setFileName("Cattle.jpg");
		attachmentsList.add(attachmentOne);

		attachmentTwo = new Attachment();
		attachmentTwo.setFileName("Apple.jpg");
		attachmentsList.add(attachmentTwo);

		attachmentThree = new Attachment();
		attachmentThree.setFileName("Beatle.jpg");
		attachmentsList.add(attachmentThree);

		attachmentFour = new Attachment();
		attachmentFour.setFileName("Ants.xml");
		attachmentsList.add(attachmentFour);

		when(attachmentManager.getLatestVersionsOfAttachments(page)).thenReturn(attachmentsList);

		assertEquals(outputText, galleryMacro.execute(params, "", pageContext));
	}

	public void testGetName()
	{
	    GalleryMacro galleryMacro = new GalleryMacro();

	    assertEquals("gallery", galleryMacro.getName());
	}

	public void testIsInLine()
	{
	    GalleryMacro galleryMacro = new GalleryMacro();

	    assertFalse(galleryMacro.isInline());
	}

	public void testHasBody()
	{
	    GalleryMacro galleryMacro = new GalleryMacro();

        assertTrue(galleryMacro.hasBody());
	}

	public void testRenderMode()
	{
	    GalleryMacro galleryMacro = new GalleryMacro();

	    assertEquals(RenderMode.ALL, galleryMacro.getBodyRenderMode());
	}
}
