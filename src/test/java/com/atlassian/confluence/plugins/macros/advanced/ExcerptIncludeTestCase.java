package com.atlassian.confluence.plugins.macros.advanced;

import junit.framework.TestCase;

/**
 * This test class is fairly short because the advanced.ExcerptIncludeMacro delegates almost entirely
 * to the xhtml.ExcerptIncludeMacro via a wrapper.
 *
 * Ideally we'd add tests here to confirm that the delegation was called correctly for each method on
 * the advanced.ExcerptIncludeMacro instance but this would require a rewrite to allow the wrapped
 * xhtml version to be mockable. Feel free to change it!
 */
public class ExcerptIncludeTestCase extends TestCase
{
    private ExcerptIncludeMacro excerptIncludeMacro;

    @Override
	protected void setUp() throws Exception
	{
        excerptIncludeMacro = new ExcerptIncludeMacro();
    }

    // Note: This method is not part of the xhtml macros interface, so does not exist in the xhtml macro test class.
	public void testHasBody()
	{
        assertEquals(false, excerptIncludeMacro.hasBody());
	}

    // Note: This method is not part of the xhtml macros interface, so does not exist in the xhtml macro test class.
	public void testSuppressMacroRenderingDuringWysiwyg()
	{
        assertEquals(true, excerptIncludeMacro.suppressMacroRenderingDuringWysiwyg());
	}
}
