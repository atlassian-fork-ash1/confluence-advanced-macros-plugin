package com.atlassian.confluence.plugins.macros.advanced;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.util.actions.AlphabeticalLabelGroupingSupport;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

import junit.framework.TestCase;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;

public class ListLabelsMacroTestCase extends TestCase
{
	private Map<String, Object> contextMap;
    private String returnText = "Execution complete";
	private List<Label> listLabels;
	
    @Mock private Map<String, Object> parameters;
	@Mock private PageContext pageContext;
	@Mock private LabelManager labelManager;
	@Mock private SpaceManager spaceManager;
	

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		contextMap = new HashMap<String, Object>();
		
		listLabels = new ArrayList<Label>();
		listLabels.add(new Label("telephone"));
		listLabels.add(new Label("zebra"));
		listLabels.add(new Label("bag"));
		listLabels.add(new Label("apple"));
	}
	
	public void testListLabelsWithOneSpaceKey() throws MacroException
	{
		String spaceKey = "ds";
		
		ListLabelsMacro listLabelsMacro = new ListLabelsMacro()
		{
			@Override
			protected Map<String, Object> getDefaultVelocityContext() {
				return contextMap;
			}

			@Override
			protected String getRenderedTemplate(Map contextMap) {
				AlphabeticalLabelGroupingSupport alphaSupport = (AlphabeticalLabelGroupingSupport) contextMap.get("alphaSupport");
				List listContext = alphaSupport.getContents();
				
				assertEquals(listLabels.get(0), listContext.get(0));
				assertEquals(listLabels.get(1), listContext.get(1));
				assertEquals(listLabels.get(2), listContext.get(2));
				assertEquals(listLabels.get(3), listContext.get(3));
				assertEquals("ds", (String) contextMap.get("spaceKey"));
				
				return returnText;
			}
		};
		listLabelsMacro.setLabelManager(labelManager);
		listLabelsMacro.setSpaceManager(spaceManager);

		when(parameters.get("spaceKey")).thenReturn(spaceKey);
		when(labelManager.getLabelsInSpace(spaceKey)).thenReturn(listLabels);
		verify(spaceManager, never()).getAllSpaces();
		
		assertEquals(returnText, listLabelsMacro.execute(parameters, "", pageContext));
	}
	
	public void testAlphabeticalLabelGroupingWithAllSpaceKey() throws MacroException
	{
		final String spaceKey = "@all";
		
		Space space = new Space();
		space.setKey("tst");
		List<Space> listSpace = new ArrayList<Space>();
		listSpace.add(space);
		
		ListLabelsMacro listLabelsMacro = new TestListLabelsMacro()
		{
			@Override
			protected String getRenderedTemplate(Map contextMap) {
				AlphabeticalLabelGroupingSupport alphaSupport = (AlphabeticalLabelGroupingSupport) contextMap.get("alphaSupport");
				List listContext = alphaSupport.getContents();
				
				assertEquals(listLabels.get(3), listContext.get(0));
				assertEquals(listLabels.get(2), listContext.get(1));
				assertEquals(listLabels.get(0), listContext.get(2));
				assertEquals(listLabels.get(1), listContext.get(3));
				assertEquals(spaceKey, (String) contextMap.get("spaceKey"));
				
				return returnText;
			}
		};
		
		when(parameters.get("spaceKey")).thenReturn(spaceKey);
		when(spaceManager.getAllSpaces()).thenReturn(listSpace);
		when(labelManager.getLabelsInSpace("tst")).thenReturn(listLabels);
		assertEquals(returnText, listLabelsMacro.execute(parameters, "", pageContext));
	}
	
	public void testListLabelsWithoutSpaceKey() throws MacroException
	{
		final String spaceKey = "tst";
		
		ListLabelsMacro listLabelsMacro = new TestListLabelsMacro()
		{
			@Override
			protected String getRenderedTemplate(Map<String, Object> contextMap) {
				AlphabeticalLabelGroupingSupport alphaSupport = (AlphabeticalLabelGroupingSupport) contextMap.get("alphaSupport");
				List listContext = alphaSupport.getContents();

				assertEquals(listLabels.get(0), listContext.get(0));
				assertEquals(listLabels.get(1), listContext.get(1));
				assertEquals(listLabels.get(2), listContext.get(2));
				assertEquals(listLabels.get(3), listContext.get(3));
				assertEquals(spaceKey, (String) contextMap.get("spaceKey"));

				return returnText;
			}
		};

		when(pageContext.getSpaceKey()).thenReturn(spaceKey);
		when(labelManager.getLabelsInSpace(spaceKey)).thenReturn(listLabels);
		verify(spaceManager, never()).getAllSpaces();
		
		assertEquals(returnText, listLabelsMacro.execute(parameters, "", pageContext));
	}

	public void testListLabelsWithExcludedLabels() throws MacroException
	{
		final String spaceKey = "tst";

		ListLabelsMacro listLabelsMacro = new TestListLabelsMacro()
		{
			@Override
			protected String getRenderedTemplate(Map<String, Object> contextMap) {
				AlphabeticalLabelGroupingSupport alphaSupport = (AlphabeticalLabelGroupingSupport) contextMap.get("alphaSupport");
				List listContext = alphaSupport.getContents();

				assertEquals(listLabels.get(2), listContext.get(0));
				assertEquals(listLabels.get(3), listContext.get(1));
				assertEquals(spaceKey, (String) contextMap.get("spaceKey"));

				return returnText;
			}
		};

		when(pageContext.getSpaceKey()).thenReturn(spaceKey);
		when(labelManager.getLabelsInSpace(spaceKey)).thenReturn(listLabels);
		verify(spaceManager, never()).getAllSpaces();
        when(parameters.get("excludedLabels")).thenReturn("  zebra , telephone ");

		assertEquals(returnText, listLabelsMacro.execute(parameters, "", pageContext));
	}

	public void testIsInline()
	{
	    ListLabelsMacro listLabelsMacro = new TestListLabelsMacro();
	    
	    assertEquals(false, listLabelsMacro.isInline());
	}
	
	public void testHasBody()
    {
        ListLabelsMacro listLabelsMacro = new TestListLabelsMacro();
        
        assertEquals(false, listLabelsMacro.hasBody());
    }
	
	public void testGetBodyRenderMode()
    {
        ListLabelsMacro listLabelsMacro = new TestListLabelsMacro();
        
        assertEquals(RenderMode.NO_RENDER, listLabelsMacro.getBodyRenderMode());
    }
	
	private class TestListLabelsMacro extends ListLabelsMacro
	{
	    public TestListLabelsMacro()
	    {
	        setLabelManager(labelManager);
	        setSpaceManager(spaceManager);
	    }
	    
	    @Override
		protected Map<String, Object> getDefaultVelocityContext() {
			return contextMap;
		}
	}
}
