package com.atlassian.confluence.plugins.macros.advanced.recentupdate.recentupdate;

import java.util.List;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.CommentUpdateItem;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.ContentUpdateItem;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultGrouper;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultUpdater;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Grouping;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultGrouper extends AbstractTestCase
{
    @MockitoAnnotations.Mock ConfluenceUserDao confluenceUserDao;
    private DefaultUpdater fred;
    private DefaultGrouper grouper;

    public void testItemsGroupedByUpdater()
    {
        SearchResult searchResult =  mock(SearchResult.class);
        when(searchResult.getLastModifier()).thenReturn(fred.getUsername());

        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));
        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));
        grouper.addUpdateItem(new CommentUpdateItem(searchResult, null, i18n, ""));

        final List<Grouping> groupings = grouper.getUpdateItemGroupings();
        assertEquals(1, groupings.size());
        assertEquals(3, groupings.get(0).size());
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        grouper = new DefaultGrouper();
        fred = new DefaultUpdater("fred", i18n);

        when(confluenceUserDao.findByUsername("fred")).thenReturn(new ConfluenceUserImpl("fred", null, null));

        ContainerContext containerContext = Mockito.mock(ContainerContext.class);  
        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("i18NBean")).thenReturn(null);
        when(containerContext.getComponent("confluenceUserDao")).thenReturn(confluenceUserDao);
    }
}
