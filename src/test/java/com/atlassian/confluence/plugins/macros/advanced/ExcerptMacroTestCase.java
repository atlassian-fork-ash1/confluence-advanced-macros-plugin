package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class ExcerptMacroTestCase extends TestCase
{
    private ContentPropertyManager contentPropertyManager;

    private ExcerptMacro excerptMacro;

    private Map<String, String> macroParams;

    private Page pageToRenderOn;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        contentPropertyManager = mock(ContentPropertyManager.class);
        excerptMacro = new TestExcerptMacro();

        macroParams = new HashMap<String, String>();
        pageToRenderOn = new Page();
    }

    public void testShowExcerpt() throws MacroException
    {
        Page otherPageAsBase = new Page();
        String body = "body";

        assertEquals(body, excerptMacro.execute(macroParams, body, new PageContext(pageToRenderOn, otherPageAsBase.toPageContext())));

        verify(contentPropertyManager, never()).setTextProperty((ContentEntityObject) anyObject(), anyString(), anyString());
    }

    public void testMacroHasBody()
    {
        assertTrue(excerptMacro.hasBody());
    }

    /* Shouldn't it be RenderMode.ALL? */
    public void testMacroBodyRenderModeIsNull()
    {
        assertNull(excerptMacro.getBodyRenderMode());
    }

    private class TestExcerptMacro extends ExcerptMacro
    {
        private TestExcerptMacro()
        {
            setContentPropertyManager(contentPropertyManager);
        }
    }
}
