package com.atlassian.confluence.plugins.macros.advanced;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.macros.advanced.search.NonViewableContentTypeSearchFilter;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.service.PredefinedSearchBuilder;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * A simple macro that performs a search and displays the result in the page.
 */
public class SearchMacro extends BaseMacro
{
	private SearchManager searchManager;
	private PredefinedSearchBuilder predefinedSearchBuilder;
    private ConfluenceActionSupport confluenceActionSupport; /* For i18n */
    private static final Logger log = LoggerFactory.getLogger(SearchMacro.class);

    private static final int DEFAULT_MAXLIMIT = 10;

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
	{
		return false;
	}

	public RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}

	public String execute(Map params, String string, RenderContext renderContext) throws MacroException
    {
		if (!(renderContext instanceof PageContext))
            throw new ClassCastException(
                    getConfluenceActionSupport().getText(
                            "search.error.macro-works-in-only-confluence",
                            new String[] {
                                    renderContext.getClass().getName(),
                                    PageContext.class.getName(),
                            }
                    )
            );
		
        String queryString = StringUtils.trimToNull((String) params.get("query"));
        String userEnteredQuery = queryString;
		if (queryString == null)
		{
			queryString = StringUtils.trimToNull((String) params.get("0"));			
		}
        if(!StringUtils.isEmpty(queryString) && ((PageContext) renderContext).getEntity() != null)
        {
            queryString = "("+queryString+") AND NOT handle: "+new HibernateHandle(((PageContext) renderContext).getEntity()).toString();
            log.info("Instrumenting query to become: " +queryString);
        }
		SearchQueryParameters searchQueryParams = new SearchQueryParameters(queryString);

		Integer maxLimit = GeneralUtil.convertToInteger(params.get("maxLimit"));
		if (maxLimit == null)
			maxLimit = DEFAULT_MAXLIMIT;

		String spaceKey = StringUtils.trimToNull((String) params.get("spacekey"));
        String type = StringUtils.trimToNull((String) params.get("type"));
        String lastModified = StringUtils.trimToNull((String) params.get("lastModified"));
        String contributor = StringUtils.trimToNull((String) params.get("contributor"));

		// convert the parameters as necessary
		searchQueryParams.setSpaceKey(spaceKey);
        
		ContentTypeEnum contentType;
        if (StringUtils.isNotBlank(type))
        {
        	contentType = ContentTypeEnum.getByRepresentation(type);
        	if (contentType == null)
        		throw new MacroException(getConfluenceActionSupport().getText("search.error.unknown-content-type"));
        	searchQueryParams.setContentType(contentType);
        }
        
        if (StringUtils.isNotBlank(lastModified))
        {
            try
            {
                long duration = DateUtils.getDuration(lastModified);
				final Date currentDateLessDuration = getCurrentDateLessDuration(duration);
				DateRangeQuery.DateRange dateRange = new DateRangeQuery.DateRange(currentDateLessDuration, null, true, false);
				searchQueryParams.setLastModified(dateRange);
			}
            catch (InvalidDurationException e)
            {
                throw new IllegalArgumentException(
                        getConfluenceActionSupport().getText(
                                "search.error.invalid-date-period",
                                new String[] { StringEscapeUtils.escapeHtml(lastModified) }));
            }
        }
        
        if (StringUtils.isNotBlank(contributor))
        {
        	searchQueryParams.setContributor(contributor);
        }

		ISearch search = buildSiteSearch(searchQueryParams, maxLimit);
		SearchResults searchResults;
		try
		{
			searchResults = searchManager.search(search);
		}
		catch (IllegalArgumentException e)
		{
			throw new MacroException(
                    getConfluenceActionSupport().getText(
                            "search.error.invalid-query-params",
                            new String[] { StringEscapeUtils.escapeHtml(searchQueryParams.toString()) }
                    ),
                    e);
		}
		catch (InvalidSearchException e)
		{
			// this shouldn't happen in production. If it did, it would imply there was a problem processing core search
			// mappers and queries
			throw new MacroException(
                    getConfluenceActionSupport().getText(
                            "search.error.fail-for-term",
                            new String[] { StringEscapeUtils.escapeHtml(searchQueryParams.getQuery()) }
                    ),
                    e);
		}

        // Remove the page this macro is on from the search results (it will always match since it has the search term
        // in its content).
        List<SearchResult> results = searchResults.getAll();

        Map<String, Object> contextMap = getDefaultVelocityContext();

		contextMap.put("searchResults", results);
        contextMap.put("totalSearchResults", results.size());
        contextMap.put("maxLimit", maxLimit);
        contextMap.put("query", userEnteredQuery);
        contextMap.put("generalUtil", new GeneralUtil());

        try
        {
            return getRenderedTemplate(contextMap);
        }
        catch (Exception e)
        {
            throw new MacroException("Error while trying to assemble the search result!", e);
        }
    }

    protected Date getCurrentDateLessDuration(long duration) {
        return new Date(System.currentTimeMillis() - (duration * 1000));
    }
 
    protected ISearch buildSiteSearch(SearchQueryParameters searchQueryParams,
            Integer maxLimit) {
        ISearch iSearch = predefinedSearchBuilder.buildSiteSearch(searchQueryParams, 0, maxLimit);

        //We create out own ISearch so we can add the NonViewableContentTypeSearchFilter.
        return new ContentSearch(
                iSearch.getQuery(),
                iSearch.getSort(),
                iSearch.getSearchFilter().and(NonViewableContentTypeSearchFilter.getInstance()),
                iSearch.getResultFilter());
    }

	public void setSearchManager(SearchManager searchManager)
	{
		this.searchManager = searchManager;
	}

	public void setPredefinedSearchBuilder(PredefinedSearchBuilder predefinedSearchBuilder)
	{
		this.predefinedSearchBuilder = predefinedSearchBuilder;
	}

    public ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }
    
    ///CLOVER:OFF
    protected Map<String, Object> getDefaultVelocityContext() {
		return MacroUtils.defaultVelocityContext();
	}

	protected String getRenderedTemplate(Map<String, Object> contextMap) {
		return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/macros/advanced/search.vm", contextMap);
	}
    ///CLOVER:OFF
}
