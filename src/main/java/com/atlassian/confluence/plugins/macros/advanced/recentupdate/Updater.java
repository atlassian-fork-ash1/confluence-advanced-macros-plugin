package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

/**
 * Represents the person who has performed an update.
 */
public interface Updater
{
    /**
     * @return html consisting the profile image of this updater wrapped in a link.
     */
    @HtmlSafe String getLinkedProfilePicture();

    /**
     * @return html consisting of the full name of this updater wrapped in a link.
     */
    @HtmlSafe String getLinkedFullName();

    @HtmlSafe String getUsername();
}
